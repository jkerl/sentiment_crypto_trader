'''
@author Julian Christopher Kerl
'''
import crypto_paper_trader as cpt
import reddit_scraper
import sentiment_analyser
import text_classifier
import twitter_scraper
#external packeges
import datetime
from datetime import timedelta
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import praw
import os
from sklearn.preprocessing import PolynomialFeatures
from sklearn.preprocessing import StandardScaler

data_path = "./data"

#User date (shouldn't be hardcoded, but who cares about data security anyways)
user_agent = 'AffScrprCrpt'
client_id = '7A0DIQw9nVvHI5Gju997wQ'
secret = 'ItaCU3Qq6eJsnVKz3SyavPLONqIMOQ'

def date_account_format(year, month, day, hour, minute, delta=0):
    '''@brief Returns a string for a given date and time that can be used with the crypto_paper_trader.Account class'''
    #create the date string. Skip the last three characters (seconds)
    s = str(datetime.datetime(year, month, day, hour, minute)+timedelta(days=delta))[:-3]
    #replace spaces
    s = s.replace(" ", "-")
    #replace :
    s = s.replace(":", "-")

    return s

def datetime_to_account_format(datetime, delta=0):
    '''@brief Returns a string for a given date and time that can be used with the crypto_paper_trader.Account class'''
    #create the date string. Skip the last three characters (seconds)
    s = str(datetime+timedelta(days=delta))[:-3]
    #replace spaces
    s = s.replace(" ", "-")
    #replace :
    s = s.replace(":", "-")

    return s

def date_reddit_scraper_format(year, month, day, hour, minute, delta=0):
    '''!@brief returns reddit timestamp for given date'''
    return int((datetime.datetime(year, month, day, hour, minute)+timedelta(days=delta)).timestamp())

def datetime_to_reddit_scraper_format(datetime, delta=0):
    '''!@brief returns reddit timestamp for given date'''
    return int((datetime+timedelta(days=delta)).timestamp())

def compare_date_account_format(start, end):
    '''!@brief returns true, if the start date is earlier than the end date'''
    #get number representation for end date
    year_end = int(end[:4])
    month_end = int(end[5:7])
    day_end = int(end[8:10])
    hour_end = int(end[11:13])
    minute_end = int(end[14:17])
    datetime_end = datetime.datetime(year_end, month_end, day_end, hour_end, minute_end)


    # get number representation for start date
    year_start = int(start[:4])
    month_start = int(start[5:7])
    day_start = int(start[8:10])
    hour_start = int(start[11:13])
    minute_start = int(start[14:17])
    datetime_start = datetime.datetime(year_start, month_start, day_start, hour_start, minute_start)

    #compare and return
    return start < end

def label_bullish_bearish(prices):
    '''!@brief labels interval as bullish, if the price dropped over the course of the interval and bearish otherwise.
    returns label and grdients'''
    gradient_prices = np.gradient(prices)
    #print(len(gradient_prices), len(prices))
    labels = []
    for i in range(len(gradient_prices)):
        if(gradient_prices[i] < 0):
            labels.append('bullish')
        else:
            labels.append('bearish')

    return labels, gradient_prices

def gaussian_proc(X, y):
    from sklearn.gaussian_process import GaussianProcessClassifier
    from sklearn.gaussian_process.kernels import RBF
    from sklearn.model_selection import train_test_split
    X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, random_state=1)
    kernel = 1.0 * RBF(1.0)
    gpc = GaussianProcessClassifier(kernel=kernel, random_state = 0).fit(X_train, y_train)
    print("gaussian process",gpc.score(X_test, y_test))
    return gpc

def neural_net(X, y):
    from sklearn.neural_network import MLPClassifier
    from sklearn.model_selection import train_test_split
    X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, random_state = 0)
    clf = MLPClassifier(alpha=0.0001,  random_state=0, max_iter=2000, solver='lbfgs').fit(X, y)
    #clf = MLPClassifier(random_state=1, max_iter=300, hidden_layer_sizes=(5,2),).fit(X_train, y_train)
    #clf.predict_proba(X_test[:1])
    #clf.predict(X_test[:5, :])
    print("neural net", clf.score(X_test, y_test))
    return clf

def decision_tree(X,y):
    from sklearn.model_selection import cross_val_score
    from sklearn.tree import DecisionTreeClassifier
    from sklearn.model_selection import train_test_split
    X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, random_state=1)
    clf = DecisionTreeClassifier(random_state=0).fit(X_train,y_train)
    print("decision tree", clf.score(X_test,y_test))
    return clf

def random_forest(X,y):
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.model_selection import train_test_split
    X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, random_state=1)
    print(len(X_train), len(X_test))
    clf = RandomForestClassifier(random_state=10).fit(X_train,y_train)
    #clf = RandomForestClassifier(max_depth=2, random_state=0).fit(X_train,y_train)
    print("random forest", clf.score(X_test,y_test))
    return clf

def collect_reddit_data(sub, start_date, end_date, interval, limit=-1, query="", save_path="."):
    '''!brief collects and saves data from given subreddit in given time frame. Before and after given as datetime.datetime.
    Returns a list of all saved files'''
    #Init reddit scraper
    reddit = praw.Reddit(client_id=client_id, client_secret=secret, user_agent=user_agent)
    reddit_collector = reddit_scraper.PSAWDataCollector(reddit)

    #Collect data
    current_date = start_date
    delta = 0
    files = []
    while (current_date < end_date):
        # offset start date by delta
        current_date = start_date + timedelta(days=delta)
        current_date_string = datetime_to_account_format(current_date)

        print('collecting data for ' + datetime_to_account_format(current_date))

        #compute timeframe
        after = datetime_to_reddit_scraper_format(current_date)
        before = datetime_to_reddit_scraper_format(current_date, interval-1)

        #read reddit submissions and extract all posts
        current_comments = reddit_collector.posts_in_timeframe(after, before, sub, limit=limit)

        #save_current comments to save_path
        path = save_path + '/comments_' + sub + '_' + current_date_string + '+' + str(interval) + '.csv'
        current_comments.to_csv(path)
        files.append('comments_' + sub + '_' + current_date_string + '+' + str(interval) + '.csv')

        # update delta
        delta = delta + interval

    return files

def preprocess_comments():
    '''!@brief TODO'''
    return

def sentiment_analysis(comments_path, file_list, save_path=".", filename="sentiments", copy_by_score=False, min_one=True):
    '''!@brief Reads all comment .csv-files from given directory and perfors sentiment analysis over them. The result
    of the sentiment analysis is then saved to save_path'''

    #init sentiment analyser
    analyser = sentiment_analyser.Analyser()

    #init sentiment DataFrame or load a previously created DataFrame
    if(os.path.isfile(save_path + '/' + filename + '.csv' )):
        sentiment_collection = pd.read_csv(save_path + '/' + filename + '.csv')
    else:
        sentiment_collection = pd.DataFrame(columns=['date', 'neu', 'pos', 'neg', 'compound'])

    #run sentiment analyis for all given files
    for f in file_list:
        #load comments
        comments = pd.read_csv(comments_path + '/' + f)
        #check if comments is empty and skip if it is
        if(len(comments) < 1):
            continue
        #run sentiment analysis
        if(copy_by_score):
            comment_list = copy_comment_by_score(comments, min_one=min_one)
        else:
            comment_list = list(comments.body)
        sentiments = analyser.vader_analysis(comment_list)
        #aggregate sentiments
        weights = []#list(comments.score)
        aggregated_sentiment = analyser.aggregate_sentiments(sentiments, weights)
        print(f)
        aggregated_sentiment['date'] = f[-22:-6]
        sentiment_collection = sentiment_collection.append(aggregated_sentiment, ignore_index=True)
        sentiment_collection.to_csv(save_path + '/' + filename + '.csv')

    return sentiment_collection

def create_comment_file_list(sub, start_date, end_date, interval):
    '''!@brief Creates list of names of comment-csvs from given parameters'''
    file_list = []
    current_date = start_date
    delta = 0
    while (current_date < end_date):
        # offset start date by delta
        current_date = start_date + timedelta(days=delta)
        current_date_string = datetime_to_account_format(current_date)

        #create file name
        file_list.append('comments_' + sub + '_' + current_date_string + '+' + str(interval) + '.csv')

        #update delta
        delta = delta + interval

    return file_list

def vector_from_sentiment_df(sentiments):
    '''!@breif Transform dataframe of sentiments to list of vectors'''
    pos = list(sentiments.pos)
    neu = list(sentiments.neu)
    neg = list(sentiments.neg)
    compound = list(sentiments.compound)
    collected_sentiments = []
    for i in range(len(sentiments)-1):
        #create vector containing the sentiments
        sentiment_vector = [pos[i], neu[i], neg[i], compound[i]]
        collected_sentiments.append(sentiment_vector)

    return collected_sentiments

def collect_crypto_prices(symbol, start_date, end_date, interval, save_path=".", filename="prices"):
    '''!@brief returns and saves list of prices for given symbol in given time frame'''
    account = cpt.Account(data_path, 'data_collector')
    current_date = start_date
    delta = 0
    prices = []
    dates = []
    while (current_date < end_date):
        # offset start date by delta
        current_date = start_date + timedelta(days=delta)
        # update delta
        delta = delta + interval
        # add dates and prices to their respective lists 'BTC-EUR'
        prices.append(account.get_price(symbol, datetime_to_account_format(current_date)))
        dates.append(datetime_to_account_format(current_date))

    gradient_prices = np.gradient(prices)
    prices_dataframe = pd.DataFrame(list(zip(dates, prices, gradient_prices)),
                                    columns=['date', 'price', 'gradient_price'])
    prices_dataframe = prices_dataframe.set_index('date')
    prices_dataframe.to_csv(save_path + '/' + filename + '.csv')

def create_training_data(sentiment_list, prices_list):
    '''!@brief Creates X, y scikit learn training data from sentiment and prices'''
    X = sentiment_list[:-1]
    y = []
    #print(len(X), len(prices_list))
    for i in range(len(X)):
        # select label
        if ((prices_list[i + 1] - prices_list[i]) < 0):
            #if bitcoin price dropped over course of intervall, label 0 -> sell
            label = 0
        else:
            label = 1

        y.append(label)

    return X, y

def create_training_data_offset(sentiment_list, prices_list, offset=1):
    '''!@brief Creates X, y scikit learn training data from sentiment and prices. Checks if trend over offset'''
    X = sentiment_list[:-offset]
    y = []
    #print(len(X), len(prices_list))
    for i in range(len(X)):
        # select label
        if ((prices_list[i + offset] - prices_list[i]) < 0):
            #if bitcoin price dropped over course of intervall, label 0 -> sell
            label = 0
        else:
            label = 1

        y.append(label)

    return X, y

def unify_vectors_from_lists(list1, list2):
    '''!@brief combines the vectors at every index for two lists, resulting in a list of combined vectors'''
    list_new = []
    i = 0
    while(i < len(list1) and i < len(list2)):
        #combine vectors from both lists
        new_vector = np.append(list1[i], list2[i])
        #add new vector to resulting list
        list_new.append(new_vector)
        #update i
        i = i + 1

    return list_new

def plot_labeled_prices(prices_df, labels, title='', show=True):
    '''!@brief creates a plot of the prices with labels for 'buy' and 'sell' '''
    # create lists with buy/sell prices for visiualisation
    date_buy = []
    date_sell = []
    price_buy = []
    price_sell = []
    for i in range(len(labels)):
        # check label and add it and its respective date to their according lists
        if (labels[i] == 0):
            date_sell.append(list(prices_df.date)[i])
            price_sell.append(list(prices_df.price)[i])
        else:
            date_buy.append(list(prices_df.date)[i])
            price_buy.append(list(prices_df.price)[i])

    plt.figure(title + ' prices')
    plt.plot(list(prices_df.date), list(prices_df.price))
    plt.scatter(date_buy, price_buy, marker='^', color='g', label='buy')
    plt.scatter(date_sell, price_sell, marker='v', color='r', label='sell')
    plt.xticks(rotation=90)
    plt.legend()
    plt.show(show)

def copy_comment_by_score(comments, min_one=True):
    '''!@brief Returns a lists that contains abs(score) copies of the comments'''
    new_comments = []
    for i in range(len(comments)):
        if(min_one):
            #add comment
            new_comments.append(list(comments.body)[i])
        #add copies
        for j in range(np.abs(list(comments.score)[i])):
            new_comments.append(list(comments.body)[i])

    return new_comments

def use_classifer_on_data(classifier, data_vectors, prices_list, starting_capital=10000, show=True, name=""):
    '''!@brief classifies given data vectors and computes the profit for a given starting capital'''
    labels = classifier.predict(data_vectors)

    current_capital = starting_capital
    current_coins = 0.0
    worth = []
    current_worth = starting_capital
    hold_only = []
    for i in range(len(labels)):
        hold_only.append((starting_capital / prices_list[0]) * prices_list[i])
        if(labels[i] == 0 and current_coins > 0):
            current_capital = current_coins * prices_list[i]
            current_coins = 0.0
            l = "sell"
            current_worth = current_capital
        elif(labels[i] == 1 and current_capital > 0):
            current_coins = current_capital / prices_list[i]
            current_capital = 0.0
            l = "buy"
            current_worth = current_coins * prices_list[i]
        else:
            current_worth = current_coins * prices_list[i] + current_capital
            l = "hold"

        #print(l, current_worth, current_coins, current_capital)
        worth.append(current_worth)

    plt.figure(name + ": worth")
    plt.plot(worth, label="worth")
    plt.plot(hold_only, label="hold only")
    plt.ylabel("Euro")
    plt.legend()
    plt.figure(name + ": price and labels")
    plt.plot(prices_list)
    plt.ylabel("Euro")
    for j in range(len(labels)):
        if(labels[j] == 0):
            plt.scatter(j, prices_list[j], color='r', marker='v', label='sell')
        else:
            plt.scatter(j, prices_list[j], color='g', marker='^', label='buy')
    #plt.legend()

    print(name, "yielded overall profit: ", worth[-1] - starting_capital, " profit in percent: ", (worth[-1] / starting_capital - 1)*100)
    plt.plot(show)

def act_by_label(labels, prices_list, starting_capital=10000, show=True, name="", ticks=[], spacing=1):
    '''!@brief classifies given data vectors and computes the profit for a given starting capital'''

    current_capital = starting_capital
    current_coins = 0.0
    worth = []
    current_worth = starting_capital
    hold_only = []
    for i in range(len(labels)):
        hold_only.append((starting_capital / prices_list[0]) * prices_list[i])
        if(labels[i] == 0 and current_coins > 0):
            current_capital = current_coins * prices_list[i]
            current_coins = 0.0
            l = "sell"
            current_worth = current_capital
        elif(labels[i] == 1 and current_capital > 0):
            current_coins = current_capital / prices_list[i]
            current_capital = 0.0
            l = "buy"
            current_worth = current_coins * prices_list[i]
        else:
            current_worth = current_coins * prices_list[i] + current_capital
            l = "hold"

        #print(l, current_worth, current_coins, current_capital)
        worth.append(current_worth)

    plt.figure(name+"-profit")
    if(len(ticks) > 0):
        plt.plot(ticks[:len(worth)], worth, label="worth")
        plt.plot(ticks[:len(hold_only)], hold_only, label="buy once and hold")
        plt.xticks(np.arange(0, len(ticks[:len(worth)]) + 1, spacing), rotation=-75)
    else:
        plt.plot(worth, label="worth")
        plt.plot(hold_only, label="buy once and hold")
    #plt.legend()
    plt.ylabel("Euro[€]")
    print(ticks)
    plt.annotate("Best Case", (ticks[len(worth) - 1], worth[-1]))
    plt.annotate("Buy once and wait", (ticks[len(worth) - 1], hold_only[-1]))
    plt.gca().spines['top'].set_visible(False)
    plt.gca().spines['right'].set_visible(False)
    plt.figure(name+" - price and labels")
    if (len(ticks) > 0):
        plt.plot(ticks[:len(prices_list)], prices_list)
        plt.xticks(np.arange(0, len(ticks[:len(prices_list)]) + 1, spacing), rotation=-75)
    else:
        plt.plot(prices_list)
    plt.ylabel("Euro")


    '''
    plt.annotate("VADER - r/Bitcoinmarkets", (dates[len(worth_best)-1], worth_classifier_BitcoinMarket[len(worth_classifier_twitter)-1]))
    plt.annotate("VADER - Twitter", (dates[len(worth_best)-1], worth_classifier_twitter[-1]))
    plt.annotate("VADER - Twitter + r/BitcoinMarkets", (dates[len(worth_best)-1], worth_classifier_combined[-1]))
    plt.annotate("BUY/SELL-Classifier - r/BitcoinMarkets", (dates[len(worth_best)-1], worth_classifier_text_BitcoinMarkets[-1]))
    plt.annotate("BUY/SELL-Classifier - Twitter", (dates[len(worth_best)-1], worth_classifier_text_twitter[-1]))
    '''


    for j in range(len(labels)):
        if(labels[j] == 0):
            plt.scatter(j, prices_list[j], color='r', marker='v', label='sell')
        else:
            plt.scatter(j, prices_list[j], color='g', marker='^', label='buy')

    print(name,"overall profit: ", worth[-1] - starting_capital, " profit in percent: ", (worth[-1] / starting_capital - 1)*100)
    print("compared to ", (hold_only[-1] / starting_capital - 1)*100, "percent or", hold_only[-1] - starting_capital, "Euros if we just bought and held that coin")
    plt.plot(show)
    return worth, hold_only

def score_from_y(y_test, y_true):
    """!@brief computes the percentage of correctly classified data points"""
    sum_correct = 0
    for i in range(len(y_test)):
        if(y_test[i] == y_true[i]):
            sum_correct = sum_correct + 1

    return sum_correct / len(y_test)

def remove_hour_minute_from_date(date):
    """!@brief Removes the hour and minute part of a date in account format"""
    return date[:-6]

def remove_hour_minute_from_date_list(date_list):
    """!@brief Removes the hour and minute part of all dates in account format in a list of dates"""
    date_list_new = []
    for d in date_list:
        date_list_new.append(remove_hour_minute_from_date(d))

    return date_list_new

def textblob():
    # tuning
    tuning = False
    # set paths
    # training data
    sentiment_path_training_data = "./data/BitcoinMarkets_Data"
    prices_path_training_data = "./data/BitcoinMarkets_Data/prices.csv"

    # test data
    sentiment_path_test_data = "./data/Test_Data_Bitcoin_Market"
    prices_path_test_data = "./data/Test_Data_Bitcoin_Market/reference_prices.csv"

    # set sentiment file
    sentiment_file_training_data = "textblob_sentiment.csv"
    #sentiment_file_training_data = "textblob_sentiment_weight_min_one_true.csv"
    #sentiment_file_training_data = "textblob_sentiment_weight_min_one_false.csv"

    # load sentiment training data
    sentiments_training_data = pd.read_csv(sentiment_path_training_data + "/" + sentiment_file_training_data)

    # extract positive and negative sentiment
    polarity_training_data = list(sentiments_training_data.polarity)
    subjectivity_training_data = list(sentiments_training_data.subjectivity)

    # create vector list for training data
    sentiment_list_training_data = unify_vectors_from_lists(polarity_training_data, subjectivity_training_data)
    sentiment_list_training_data = np.array(polarity_training_data).reshape(-1, 1)
    #sentiment_list_training_data = np.array(polarity_training_data).reshape(-1,1)

    # load finacial training data
    prices_training_data = pd.read_csv(prices_path_training_data)
    prices_list_training_data = list(prices_training_data.price)

    # create training data
    X, y = create_training_data(sentiment_list_training_data, prices_list_training_data)
    ##Feature scaling
    from sklearn.preprocessing import StandardScaler
    # train scaler
    scaler = StandardScaler()
    scaler.fit(X)
    X = scaler.transform(X)

    # plot sentiments and prices
    x_vals = list(prices_training_data.date)
    plt.figure('sentiment')
    plt.plot(x_vals, polarity_training_data, color='g', label="positive")
    plt.plot(x_vals, subjectivity_training_data, color='y', label="negative")
    plt.legend()
    plt.xticks(rotation=90)
    plt.figure('prices')
    plt.plot(x_vals, prices_list_training_data, color='black')
    plt.xlabel("Euro")

    # plot double y axis plot

    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax1.plot(x_vals, polarity_training_data, color='g')
    ax1.plot(x_vals, subjectivity_training_data, color='y')
    ax1.set_ylabel('sentiment')

    ax2 = ax1.twinx()
    ax2.plot(x_vals, prices_list_training_data, 'black')
    ax2.set_ylabel('price in Euro', color='black')
    # plt.show()

    # train classifiers


    #gp = gaussian_proc(X, y)
    nn = neural_net(X, y)
    #dt = decision_tree(X, y)
    #rf = random_forest(X, y)

    # load sentiment test data
    sentiments_test_data = pd.read_csv(sentiment_path_test_data + "/" + sentiment_file_training_data)
    polarity_test_data = list(sentiments_test_data.polarity)
    subjectivity_test_data = list(sentiments_test_data.subjectivity)

    # load prices test data
    prices_test_data = pd.read_csv(prices_path_test_data)
    prices_list_test_data = list(prices_test_data.price)

    # create data points from loaded data
    sentiment_list_test_data = unify_vectors_from_lists(polarity_test_data, subjectivity_test_data)
    sentiment_list_test_data = np.array(polarity_test_data).reshape(-1, 1)
    sentiment_list_test_data = scaler.transform(sentiment_list_test_data)
    #sentiment_list_test_data = np.array(polarity_test_data).reshape(-1, 1)

    # use classifier and plot results
    #use_classifer_on_data(dt, sentiment_list_test_data, prices_list_test_data, starting_capital=10000, show=False,
    #                      name="dt")
    use_classifer_on_data(nn, sentiment_list_test_data, prices_list_test_data, starting_capital=10000, show=False,
                          name="nn")
    #use_classifer_on_data(gp, sentiment_list_test_data, prices_list_test_data, starting_capital=10000, show=False,
    #                      name="gp")
    #use_classifer_on_data(rf, sentiment_list_test_data, prices_list_test_data, starting_capital=10000, show=False,
     #                     name="rf")
    # plot_labeled_prices(prices_test, y_test, "btc-eur test", show=False)
    X_test, y_test = create_training_data(sentiment_list_test_data, prices_list_test_data)
    act_by_label(y_test, prices_list_test_data, starting_capital=10000, show=True)
    plt.show()

def test():
    print("test")
    '''
    trader = cpt.crypto_paper_trader(".")
    trader.create_account("test_account", 100000.0)
    trader.deposit_to_account("test_account", 1000)
    trader.withdraw_from_account("test_account", 100)


    trader.execute_order(order, "test_account")
    trader.load_accounts()
    
    print(date_account_format(2020, 1, 1, 0, 0))
    order = cpt.order('buy', 'BTC-EUR', 1, '2020-06-01-00-00')
    account = cpt.Account('.', 'test2')
    print(account.get_price('BTC-EUR', '2020-06-01-00-00'))
    account.deposit(100000)
    account.execute_order(order)
    order_2 = cpt.order('sell', 'BTC-EUR', 1, '2021-06-01-00-00')
    account.execute_order(order_2)
    account.save()
    account.load()
    '''
    account = cpt.Account(data_path, 'data_collector')
    analyser = sentiment_analyser.Analyser()

    #pull price data for timeframe
    start_date = date_account_format(2019, 1, 1, 0, 0)
    end_date = date_account_format(2020, 1, 1, 0, 0)
    #intervall between data points in days
    interval = 7
    print(start_date, end_date)
    print(compare_date_account_format(start_date, end_date))
    current_date = start_date
    delta = 0
    prices = []
    dates = []
    '''
    #for all intervals beween start date abd end date
    while(compare_date_account_format(current_date, end_date)):
        #offset start date by delta
        current_date = date_account_format(2019, 1, 1, 0, 0, delta)
        #update delta
        delta = delta + interval
        #add dates and prices to their respective lists
        prices.append(account.get_price('BTC-EUR', current_date))
        dates.append(current_date)

    gradient_prices = np.gradient(prices)
    prices_dataframe = pd.DataFrame(list(zip(dates, prices, gradient_prices)), columns=['date', 'price', 'gradient_price'])
    prices_dataframe = prices_dataframe.set_index('date')
    prices_dataframe.to_csv(data_path + '/prices.csv')
    
    #get reddit posts
    sub = "Bitcoin"
    limit = 100


    reddit = praw.Reddit(client_id=client_id, client_secret=secret, user_agent=user_agent)
    reddit_collector = reddit_scraper.PSAWDataCollector(reddit)
    analyser = sentiment_analyser.Analyser()

    #get reddit posts and rate their sentiment
    sentiment_collection = pd.DataFrame(columns=['date', 'neu', 'pos', 'neg', 'compound'])
    comments = pd.DataFrame(columns=['id', 'submission_id', 'score', 'body'])
    current_date = start_date
    delta = 0
    while (compare_date_account_format(current_date, end_date)):
        # offset start date by delta
        current_date = date_account_format(2019, 1, 1, 0, 0, delta)
        #get reddit
        
        after = date_reddit_scraper_format(2019, 7, 16, 0, 0, delta)
        before = date_reddit_scraper_format(2019, 7, 16, 0, 0, delta+interval)
        print(current_date)
        current_comments = reddit_collector.posts_in_timeframe(after, before, sub, limit=limit)
        comments.append(current_comments)
        current_comments.to_csv(data_path + '/comments_'+current_date+'.csv')
        #print(current_comments)
        
        comments = pd.read_csv(data_path + '/comments_'+current_date+'.csv')
        sentiments = analyser.vader_analysis(list(comments.body))
        aggregated_sentiment = analyser.aggregate_sentiments(sentiments, [])
        aggregated_sentiment['date'] = current_date
        print(sentiments)
        sentiment_collection = sentiment_collection.append(aggregated_sentiment, ignore_index=True)
        sentiment_collection.to_csv(data_path + '/sentiments.csv')

        # update delta
        delta = delta + interval

    print(comments)
    sentiment_collection.to_csv(data_path + '/sentiments.csv')


    
    label_bullish_bearish(prices)
    plt.figure('prices')
    plt.plot(dates, prices)
    plt.plot(dates, gradient_prices)
    plt.xticks(rotation=90)
    plt.show()
    '''
    #sentiments weight by score
    sentiment_collection = pd.DataFrame(columns=['date', 'neu', 'pos', 'neg', 'compound'])
    while (compare_date_account_format(current_date, end_date)):
        # offset start date by delta
        current_date = date_account_format(2019, 1, 1, 0, 0, delta)
        print("computing sentiments for " + current_date, end='\r')
        comments = pd.read_csv(data_path + '/comments_' + current_date + '.csv')
        #comments_sorted = comments.sort_values(by='score', ascending=False, key=abs)
        #print(comments_sorted)
        sentiments = analyser.vader_analysis(list(comments.body))
        #weights = list(comments.score)[:500]
        weights = []
        aggregated_sentiment = analyser.aggregate_sentiments(sentiments, weights)
        aggregated_sentiment['date'] = current_date
        sentiment_collection = sentiment_collection.append(aggregated_sentiment, ignore_index=True)
        sentiment_collection.set_index('date')
        sentiment_collection.to_csv(data_path + '/sentiments.csv')

        # update delta
        delta = delta + interval

    #create training data
    #load sentiment data
    sentiments = pd.read_csv(data_path + '/sentiments.csv')
    #load prices
    prices = pd.read_csv(data_path + '/prices.csv')

    #create DataFrame for labels and sentiment data. Sentiment data is labeled 1 or 'buy' if the price increased in the following week
    pos = list(sentiments.pos)
    neu = list(sentiments.neu)
    neg = list(sentiments.neg)
    compound = list(sentiments.compound)
    gradient_prices = list(prices.gradient_price)

    training_data = pd.DataFrame(columns=['sentiments', 'label'])

    #data for visualisation
    buy_pos = []
    buy_neu = []
    buy_neg = []

    sell_pos = []
    sell_neu = []
    sell_neg = []

    label_buy = []
    label_sell = []
    date_buy = []
    date_sell = []

    for i in range(len(sentiments)-1):
        #create vector containing the sentiments
        sentiments = [pos[i], neu[i], neg[i], compound[i]]

        #select label
        if((list(prices.price)[i+1] - list(prices.price)[i]) < 0):
            label = 0

            buy_pos.append(pos[i])
            buy_neu.append(compound[i])
            buy_neg.append(neg[i])

            label_sell.append(list(prices.price)[i])
            date_sell.append(list(prices.date)[i])
        else:
            label = 1

            sell_pos.append(pos[i])
            sell_neu.append(compound[i])
            sell_neg.append(neg[i])

            label_buy.append(list(prices.price)[i])
            date_buy.append(list(prices.date)[i])

        training_data = training_data.append({'sentiments':sentiments, 'label':label}, ignore_index=True)


    training_data.to_csv(data_path + '/training_data.csv')
    X = list(training_data.sentiments)
    y = list(training_data.label)

    print("gaussion process")
    gaussian_proc(X, y)
    print("neural network")
    neural_net(X, y)
    print("decision tree")
    decision_tree(X, y)


    plt.figure('prices')
    plt.plot(list(prices.date), list(prices.price))
    plt.xticks(rotation=90)
    plt.scatter(date_sell, label_sell, marker='o')
    plt.scatter(date_buy, label_buy, marker='^')
    #plt.show()

    fig = plt.figure("datapoints")
    ax = fig.add_subplot(projection='3d')
    ax.set_xlabel('Positive')
    ax.set_ylabel('Neutral')
    ax.set_zlabel('Negative')
    ax.scatter(sell_pos, sell_neu, sell_neg, marker='o')
    ax.scatter(buy_pos, buy_neu, buy_neg, marker='^')
    plt.show()

def test2():
    #Set paths
    #save_path_comments =  "./data/Bitcoin_Comments_2018_2020_5d"
    save_path_comments =  "./data/BitcoinMarkets_Comments_2020_2021_5d"
    #save_path_comments =  "./data/Bitcoin_Comments_2018_2020_5d"
    #save_path_comments =  "./data/BitcoinMarkets_Comments_2018_2020_5d"

    #Set reddit params
    sub = 'BitcoinMarkets'
    #start_date = datetime.datetime(2018, 1, 1)
    #start_date = datetime.datetime(2020, 11, 23)
    start_date = datetime.datetime(2020, 7, 1)
    end_date = datetime.datetime(2021, 3, 8)
    interval = 5
    limit = 100

    #Collect data
    #comment_files = collect_reddit_data(sub, start_date, end_date, interval, limit=limit, query="", save_path=save_path_comments)

    #run sentiment analyis
    #BitcoinMarket training comments

    save_path_comments = "./data/BitcoinMarkets_Comments_2018_2020_5d"
    save_path_sentiment = "./data/BitcoinMarkets_Data"
    save_path_test_data = ""
    #start_date = datetime.datetime(2018, 1, 1)
    #end_date = datetime.datetime(2020, 7, 1)
    start_date = datetime.datetime(2020, 7, 1)
    end_date = datetime.datetime(2021, 7, 1)
    sub = 'BitcoinMarkets'
    interval = 5
    '''    save_path_comments_btc =  "./data/Bitcoin_Comments_2018_2020_5d"
    save_path_sentiment_btc = "./data/Bitcoin_Data"
    start_date = datetime.datetime(2020, 7, 1)
    end_date = datetime.datetime(2021, 7, 1)
    sub_btc = 'Bitcoin'
    interval_btc = 5
    #create file list
    file_list_BitcoinMarkets = create_comment_file_list(sub, start_date, end_date, interval)
    sentiment = pd.DataFrame(columns=('polarity', 'subjectivity'))
    sentiment1 = pd.DataFrame(columns=('polarity', 'subjectivity'))
    sentiment2 = pd.DataFrame(columns=('polarity', 'subjectivity'))
    sentiment3 = pd.DataFrame(columns=('polarity', 'subjectivity'))
    '''
    analyser = sentiment_analyser.Analyser()
    start_date = datetime.datetime(2020, 7, 1)
    end_date = datetime.datetime(2021, 3, 8)
    file_list_ref_data = create_comment_file_list(sub, start_date, end_date, interval)
    comments_path = "./data/BitcoinMarkets_Comments_2020_2021_5d"
    #collect_crypto_prices('BTC-EUR', start_date, end_date, interval, save_path=".", filename='reference_prices')
    #not weighed
    #sentiment_analysis(comments_path, file_list_ref_data, save_path=save_path_sentiment, filename="vader_sentiments", copy_by_score=False)
    #sentiment_analysis(comments_path, file_list_ref_data, save_path=save_path_sentiment, filename="vader_sentiments_weight_min_one_true", copy_by_score=True, min_one=True)
    #sentiment_analysis(comments_path, file_list_ref_data, save_path=save_path_sentiment, filename="vader_sentiments_weight_min_one_false", copy_by_score=True, min_one=False)
    test_path = './data/Test_Data_Bitcoin_Market'
    '''
    sentiment = pd.DataFrame(columns=('polarity', 'subjectivity'))
    sentiment1 = pd.DataFrame(columns=('polarity', 'subjectivity'))
    sentiment2 = pd.DataFrame(columns=('polarity', 'subjectivity'))
    sentiment3 = pd.DataFrame(columns=('polarity', 'subjectivity'))
    for f in file_list_ref_data:
        print(f, end='\r')
        c = pd.read_csv(comments_path + '/' + f)
        c_weigth_min_one = copy_comment_by_score(c, min_one=True)
        c_weigth = copy_comment_by_score(c, min_one=False)
        sentiment1 = sentiment1.append(analyser.textblob_sentiment_analysis(list(c.body), aggregate=True), ignore_index=True)
        sentiment2 = sentiment2.append(analyser.textblob_sentiment_analysis(c_weigth_min_one, aggregate=True), ignore_index=True)
        sentiment3 = sentiment3.append(analyser.textblob_sentiment_analysis(c_weigth, aggregate=True), ignore_index=True)
        sentiment1.to_csv(test_path + '/test_data_textblob_sentiment.csv')
        sentiment2.to_csv(test_path + '/test_data_textblob_sentiment_weight_min_one_true.csv')
        sentiment3.to_csv(test_path + '/test_data_textblob_sentiment_weight_min_one_false.csv')
    '''
    #sentiment.plot('polarity', 'subjectivity', kind='scatter')
    #sentiment = pd.read_csv(save_path_sentiment + '/textblob_sentiment_weight_min_one_false.csv')
    sentiment = pd.read_csv(save_path_sentiment + '/textblob_sentiment_weight_min_one_false.csv')
    #sentiment = pd.read_csv(save_path_sentiment + '/textblob_sentiment.csv')
    #plt.figure('pplarity')
    #plt.plot(np.gradient(np.fft.fft(np.asarray(list(sentiment.polarity)))[1:]))
    #plt.figure('subjectivity')
    #plt.plot(list(sentiment.subjectivity))

    #print(file_list_BitcoinMarkets)
    #run sentiment analysis
    #sentiment_collection = sentiment_analysis(save_path_comments, file_list_BitcoinMarkets, save_path=save_path_sentiment)

    #collect_crypto_prices('BTC-EUR', start_date, end_date, interval, save_path=save_path_sentiment)
    #sentiment_collection_btc = pd.read_csv(save_path_sentiment_btc + '/sentiments.csv')
    #sentiment_vectors_btc = vector_from_sentiment_df(sentiment_collection_btc)

    #sentiment_collection = pd.read_csv(save_path_sentiment + '/textblob_sentiments.csv')
    #sentiment_vectors = vector_from_sentiment_df(sentiment_collection)
    prices_df = pd.read_csv(save_path_sentiment + '/prices.csv')

    #sentiment_collection = sentiment_collection_btc

    #pos = list(sentiment_collection.neu)
    #neg = list(sentiment_collection.neg)
    #compound = list(sentiment_collection.neg)

    gradient_prices = np.append(0, list(np.array(prices_df.price)[1:] - np.array(prices_df.price)[:-1]))
    #plt.figure('gradient')
    #plt.plot(list(prices_df.price))
    #plt.plot(gradient_prices)
    #plt.plot()
    #combined_vectors = unify_vectors_from_lists(sentiment_vectors, sentiment_vectors_btc)
    textblob_vectors = unify_vectors_from_lists(list(sentiment.polarity), list(sentiment.subjectivity))
    #textblob_vectors = unify_vectors_from_lists(textblob_vectors, list(prices_df.gradient_price))
    #textblob_vectors = unify_vectors_from_lists(textblob_vectors, gradient_prices)
    #textblob_vectors = unify_vectors_from_lists(textblob_vectors, list(prices_df.price))
    #X, y = create_training_data(combined_vectors, list(prices_df.price))
    X, y = create_training_data_offset(textblob_vectors, list(prices_df.price), offset=1)

    #X = np.abs(np.fft.fft(np.asarray(list(sentiment.polarity)))[1:]).reshape(-1, 1)
    #X = list(sentiment.polarity)[1:].reshape(-1, 1)
    plot_labeled_prices(prices_df, y, "btc-eur", show=False)
    '''
    plt.figure("textblob")
    for i in range(len(y)):
        x = [list(sentiment.polarity)[i], list(sentiment.subjectivity)[i]]
        if (y[i] == 1):
            plt.scatter([x[0]], [x[1]], color='g')
        else:
            plt.scatter([x[0]], [x[1]], color='r')
    '''
    #Train classifiers
    gp = gaussian_proc(X, y)
    nn = neural_net(X, y)
    dt = decision_tree(X, y)
    #Test perfomance
    test_path = './data/Test_Data_Bitcoin_Market'
    #comment_set = 'test_data_textblob_sentiment.csv'
    comment_set = 'test_data_textblob_sentiment_weight_min_one_false.csv'
    sentiment_test = pd.read_csv(test_path + '/' + comment_set)
    prices_test = pd.read_csv(test_path + '/reference_prices.csv')
    #create datapoints for classifier
    gradient_prices_test = np.append(0, list(np.array(prices_test.price)[1:] - np.array(prices_test.price)[:-1]))
    textblob_vectors_test = unify_vectors_from_lists(list(sentiment_test.polarity), list(sentiment_test.subjectivity))
    #textblob_vectors_test = unify_vectors_from_lists(textblob_vectors_test, gradient_prices_test)


    use_classifer_on_data(dt, textblob_vectors_test, list(prices_test.price), starting_capital=10000, show=False)

    X_test, y_test = create_training_data_offset(textblob_vectors_test, list(prices_test.price), offset=1)
    #plot_labeled_prices(prices_test, y_test, "btc-eur test", show=False)
    act_by_label(y_test, list(prices_test.price), starting_capital=10000, show=True)
    plt.show()
    '''
    pos_fft_real = np.imag(np.fft.fft(np.asarray(pos)))
    pos_fft_imag = np.imag(np.fft.fft(np.asarray(pos)))
    neg_fft_real = np.imag(np.fft.fft(np.asarray(neg)))
    neg_fft_imag = np.imag(np.fft.fft(np.asarray(neg)))
    pos_fft = unify_vectors_from_lists(pos_fft_real[1:-1], pos_fft_imag[1:-1])
    neg_fft = unify_vectors_from_lists(neg_fft_real[1:-1], neg_fft_imag[1:-1])
    
    pos_fft = np.fft.fft(np.asarray(pos))
    neg_fft = np.fft.fft(np.asarray(neg))
    
    pos_fft_real = np.real(np.fft.fft(np.asarray(pos)))
    pos_fft_imag = np.imag(np.fft.fft(np.asarray(pos)))
    neg_fft_real = np.real(np.fft.fft(np.asarray(neg)))
    neg_fft_imag = np.imag(np.fft.fft(np.asarray(neg)))
    pos_fft = unify_vectors_from_lists(pos_fft_real[1:-1], pos_fft_imag[1:-1])
    neg_fft = unify_vectors_from_lists(neg_fft_real[1:-1], neg_fft_imag[1:-1])
    X_fft = unify_vectors_from_lists(pos_fft, neg_fft)

    pos_fft = np.abs(np.fft.fft(np.asarray(pos)))
    neg_fft = np.abs(np.fft.fft(np.asarray(neg)))
    X_fft = unify_vectors_from_lists(pos_fft[1:-1], neg_fft[1:-1])
    y_fft = y
    sentiment_label = []
    plt.figure("scatter pos neg fft")
    for i in range(len(X_fft)):
        x = X_fft[i]
        if(y[i] == 1):
            plt.scatter([x[0]], [x[1]], color='g')
        else:
            plt.scatter([x[0]], [x[1]], color='r')
        sentiment_label.append([x[0]])
    plt.show()

    #X_fft = sentiment_label
    gaussian_proc(X_fft, y_fft)
    neural_net(X_fft, y_fft)
    decision_tree(X_fft, y_fft)
    plt.figure("sentiment")
    #plt.plot(list(prices_df.date), compound, label='compound')
    #plt.plot(list(prices_df.date), neu, label='neutral')
    plt.plot(list(prices_df.date), pos, label='positive')
    plt.plot(list(prices_df.date), neg, label='negative')
    plt.xticks(rotation=90)
    plt.legend()

    plt.figure("sentiment fast fourier")
    plt.plot(list(prices_df.date)[1:], pos_fft[1:], label='positve', color='g')
    plt.plot(list(prices_df.date)[1:], neg_fft[1:], label='negative', color='r')
    plt.legend()
    plt.xticks(rotation=90)
    plot_labeled_prices(prices_df, y, title='btc-eur', show=False)
    plt.show()
    
    sentiment_vectors = vector_from_sentiment_df(sentiment_collection)

    prices_df = pd.read_csv(save_path_sentiment + '/prices.csv')

    X, y = create_training_data(sentiment_vectors, list(prices_df.price))
    gaussian_proc(X, y)
    neural_net(X, y)
    decision_tree(X, y)
    
    dates = list(prices_df.date)
    pos_buy = []
    neg_buy = []
    neu_buy = []
    date_buy = []
    pos_sell = []
    neg_sell = []
    neu_sell = []
    date_sell = []
    for i in range(len(X)):
        if(y[i] == 1):
            pos_buy.append(X[i][0])
            neg_buy.append(X[i][1])
            neu_buy.append(X[i][2])
            date_buy.append(dates[i])
        else:
            pos_sell.append(X[i][0])
            neg_sell.append(X[i][1])
            neu_sell.append(X[i][2])
            date_sell.append(dates[i])


    fig = plt.figure("datapoints")
    ax = fig.add_subplot(projection='3d')
    ax.set_xlabel('Positive')
    ax.set_ylabel('Neutral')
    ax.set_zlabel('Negative')
    ax.scatter(pos_sell, neu_sell, neg_sell, marker='o')
    ax.scatter(pos_buy, neu_buy, neg_buy, marker='o')
    plt.show()
    '''

def vader():
    #tuning
    tuning = False
    #set paths
    #training data
    sentiment_path_training_data = "./data/BitcoinMarkets_Data"
    prices_path_training_data = "./data/BitcoinMarkets_Data/prices.csv"

    #test data
    sentiment_path_test_data = "./data/Test_Data_Bitcoin_Market"
    prices_path_test_data = "./data/Test_Data_Bitcoin_Market/reference_prices.csv"



    #set sentiment file
    sentiment_file_training_data = "vader_sentiments.csv"
    #sentiment_file_training_data = "vader_sentiments_weight_min_one_true.csv"
    #sentiment_file_training_data = "vader_sentiments_weight_min_one_false.csv"

    #load sentiment training data
    sentiments_training_data = pd.read_csv(sentiment_path_training_data + "/" + sentiment_file_training_data)

    #extract positive and negative sentiment
    positive_training_data = list(sentiments_training_data.pos)
    negative_training_data = list(sentiments_training_data.neg)
    compound_training_data = list(sentiments_training_data.compound)
    neutral_training_data = list(sentiments_training_data.neu)


    #create vector list for training data
    sentiment_list_training_data = unify_vectors_from_lists(positive_training_data, negative_training_data)
    #sentiment_list_training_data = unify_vectors_from_lists(sentiment_list_training_data, neutral_training_data)
    #sentiment_list_training_data = unify_vectors_from_lists(sentiment_list_training_data, compound_training_data)
    #sentiment_list_training_data = np.array(negative_training_data).reshape(-1,1)
    if(tuning):
        #tuning
        positive_tuned = np.abs(np.fft.fft(np.asarray(positive_training_data)))[1:]
        negative_tuned = np.abs(np.fft.fft(np.asarray(negative_training_data)))[1:]
        sentiment_list_training_data = unify_vectors_from_lists(positive_tuned, negative_tuned)

    #load finacial training data
    prices_training_data = pd.read_csv(prices_path_training_data)
    prices_list_training_data = list(prices_training_data.price)

    #create training data
    X, y = create_training_data(sentiment_list_training_data, prices_list_training_data)
    ##Feature scaling
    #from sklearn.preprocessing import StandardScaler
    # train scaler
    scaler = StandardScaler()
    scaler.fit(X)
    X = scaler.transform(X)
    #X = poly.fit_transform(X)

    #plot sentiments and prices
    x_vals = list(prices_training_data.date)
    plt.figure('sentiment')
    plt.plot(x_vals, positive_training_data, color='g', label="positive")
    plt.plot(x_vals, negative_training_data, color='r', label="negative")
    #plt.plot(x_vals, compound_training_data, color='b', label="compound")
    plt.legend()
    plt.xticks(rotation=90)
    plt.figure('prices')
    plt.plot(x_vals, prices_list_training_data, color='black')
    plt.xlabel("Euro")

    #plot double y axis plot
    '''
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    if(tuning):
        positive_tuned = np.abs(np.fft.fft(np.asarray(positive_training_data)))[1:]
        negative_tuned = np.abs(np.fft.fft(np.asarray(negative_training_data)))[1:]
        compound_tuned = np.abs(np.fft.fft(np.asarray(compound_training_data)))[1:]
        ax1.plot(x_vals[1:], compound_tuned, color='b')
        ax1.plot(x_vals[1:], positive_tuned, color='r')
        ax1.plot(x_vals[1:], negative_tuned, color='g')
    else:
        ax1.plot(x_vals, positive_training_data, color='g')
        ax1.plot(x_vals, negative_training_data, color='r')
        ax1.plot(x_vals, compound_training_data, color='b')
    ax1.set_ylabel('sentiment')

    ax2 = ax1.twinx()
    if(tuning):
        ax2.plot(x_vals[1:], prices_list_training_data[1:], 'black')
    else:
        ax2.plot(x_vals, prices_list_training_data, 'black')
    ax2.set_ylabel('price in Euro', color='black')
    #plt.show()
    '''
    #train classifiers
    #gp = gaussian_proc(X, y)
    nn = neural_net(X, y)
    #dt = decision_tree(X, y)
    #rf = random_forest(X, y)

    #load sentiment test data
    sentiments_test_data = pd.read_csv(sentiment_path_test_data + "/" + sentiment_file_training_data)
    positive_test_data = list(sentiments_test_data.pos)
    negative_test_data = list(sentiments_test_data.neg)
    compound_test_data = list(sentiments_test_data.compound)
    neutral_test_data = list(sentiments_test_data.neu)

    #load prices test data
    prices_test_data = pd.read_csv(prices_path_test_data)
    prices_list_test_data = list(prices_test_data.price)

    #create data points from loaded data
    sentiment_list_test_data = unify_vectors_from_lists(positive_test_data, negative_test_data)
    #sentiment_list_test_data = unify_vectors_from_lists(sentiment_list_test_data, neutral_test_data)
    #sentiment_list_test_data = unify_vectors_from_lists(sentiment_list_test_data, compound_test_data)
    ##Feature scaling
    sentiment_list_test_data = scaler.transform(sentiment_list_test_data)
    #sentiment_list_test_data = poly.fit_transform(sentiment_list_test_data)
    #sentiment_list_test_data = np.array(negative_test_data).reshape(-1,1)

    if(tuning):
        # tuning
        positive_tuned = np.abs(np.fft.fft(np.asarray(positive_test_data)))[1:]
        negative_tuned = np.abs(np.fft.fft(np.asarray(negative_test_data)))[1:]
        sentiment_list_test_data = unify_vectors_from_lists(positive_tuned, negative_tuned)

    #use classifier and plot results
    #use_classifer_on_data(dt, sentiment_list_test_data, prices_list_test_data, starting_capital=10000, show=False, name="dt")
    use_classifer_on_data(nn, sentiment_list_test_data, prices_list_test_data, starting_capital=10000, show=False, name="nn")
    #use_classifer_on_data(gp, sentiment_list_test_data, prices_list_test_data, starting_capital=10000, show=False, name="gp")
    #use_classifer_on_data(rf, sentiment_list_test_data, prices_list_test_data, starting_capital=10000, show=False, name="rf")
    # plot_labeled_prices(prices_test, y_test, "btc-eur test", show=False)
    X_test, y_test = create_training_data(sentiment_list_test_data, prices_list_test_data)
    act_by_label(y_test, prices_list_test_data, starting_capital=10000, show=True)
    plt.show()

def create_vader_data():
    # crate vader training data sentiment
    # start and end date
    start_date = datetime.datetime(2018, 1, 1)
    end_date = datetime.datetime(2020, 7, 1)
    interval = 5
    # paths
    comments_path = "./data/BitcoinMarkets_Comments_2018_2020_5d"
    save_path_sentiment = "./data/BitcoinMarkets_Data/"

    # create file list
    file_list_ref_data = create_comment_file_list("BitcoinMarkets", start_date, end_date, interval)

    # run analysis
    sentiment_analysis(comments_path, file_list_ref_data, save_path=save_path_sentiment, filename="vader_sentiments",
                       copy_by_score=False)
    sentiment_analysis(comments_path, file_list_ref_data, save_path=save_path_sentiment,
                       filename="vader_sentiments_weight_min_one_true", copy_by_score=True, min_one=True)
    sentiment_analysis(comments_path, file_list_ref_data, save_path=save_path_sentiment,
                       filename="vader_sentiments_weight_min_one_false", copy_by_score=True, min_one=False)

def text_classifier_with_training_BitcoinMarket():
    #set paths
    #prices_path_training_data = "./data/BitcoinMarkets_Data/prices.csv"
    comments_path_train = "./data/BitcoinMarkets_Comments_2018_2020_5d"
    prices_path_training_data = "./data/Bitcoin_Data/prices.csv"
    #comments_path_train = "./data/Bitcoin_Comments_2018_2020_5d"

    #comments_list
    #sub = "Bitcoin"
    sub = "BitcoinMarkets"
    interval = 5
    start_date = datetime.datetime(2018, 1, 1)
    end_date = datetime.datetime(2020, 7, 1)
    file_list_training_data = create_comment_file_list(sub, start_date, end_date, interval)

    #create document list from comments
    documents_train = []
    for f in file_list_training_data:
        print("processing", f, end='\r')
        #read comment file
        comments = pd.read_csv(comments_path_train + "/" + f)
        #create document from comments and add to documents list
        documents_train.append(text_classifier.comment_df_to_document(comments))
    print('\n')
    #create text classifier
    classifier = text_classifier.TextClassifier(path="./text_classifiers", name="text_classifier_BitcoinMarkets")

    #load prices
    prices = pd.read_csv(prices_path_training_data)

    #create training data
    D_train, y_train = create_training_data(documents_train, list(prices.price))

    #preprocess
    X_train = classifier.process_list_of_documents(D_train)

    #train classifier
    classifier.fit(X_train, y_train)

    #save classifier
    classifier.save_model()

    #load test data
    # set paths
    comments_path_test = "./data/BitcoinMarkets_Comments_2020_2021_5d"
    #prices_path_test_data = "./data/Test_Data_Bitcoin_Market/reference_prices.csv"
    #comments_path_test = "./data/Bitcoin_Comments_2020_2021_5d"
    prices_path_test_data = "./data/Test_Data_Bitcoin_Market/reference_prices.csv"

    # comments_list
    start_date = datetime.datetime(2020, 7, 1)
    end_date = datetime.datetime(2021, 3, 8)
    file_list_test_data = create_comment_file_list(sub, start_date, end_date, interval)

    # create document list from comments
    documents_test = []
    for f in file_list_test_data:
        print("processing", f, end='\r')
        # read comment file
        comments = pd.read_csv(comments_path_test + "/" + f)
        # create document from comments and add to documents list
        documents_test.append(text_classifier.comment_df_to_document(comments))
    print('\n')
    # load prices
    prices_test = pd.read_csv(prices_path_test_data)

    # create training data
    D_test, y_test = create_training_data(documents_test, list(prices_test.price))

    # preprocess
    X_test = classifier.process_list_of_documents(D_test)
    X_test = classifier.scale_transform(X_test)

    #use_classifer_on_data(classifier.classifier, X_test, list(prices_test.price), starting_capital=10000, show=False,
    #                      name="text classifier")
    # plot_labeled_prices(prices_test, y_test, "btc-eur test", show=False)
    y_pred = classifier.classifier.predict(X_test)
    print("score for test data" , score_from_y(y_pred, y_test))
    worth_best, hold_only = act_by_label(y_test, list(prices_test.price), starting_capital=10000, show=False, name="best case")
    worth_classifier, hold_only = act_by_label(y_pred, list(prices_test.price), starting_capital=10000, show=True, name="text_classifier")
    plt.show()

    return worth_classifier, worth_best, hold_only

def text_classifier_load_model_BitcoinMarket():
    #set paths
    #prices_path_training_data = "./data/BitcoinMarkets_Data/prices.csv"
    comments_path_train = "./data/BitcoinMarkets_Comments_2018_2020_5d"
    prices_path_training_data = "./data/Bitcoin_Data/prices.csv"
    #comments_path_train = "./data/Bitcoin_Comments_2018_2020_5d"
    classifier = text_classifier.TextClassifier(path="./text_classifiers", name="text_classifier_BitcoinMarkets")
    classifier.load_model()
    #comments_list
    #sub = "Bitcoin"
    sub = "BitcoinMarkets"
    interval = 5
    start_date = datetime.datetime(2018, 1, 1)
    end_date = datetime.datetime(2020, 7, 1)
    file_list_training_data = create_comment_file_list(sub, start_date, end_date, interval)

    #load test data
    # set paths
    comments_path_test = "./data/BitcoinMarkets_Comments_2020_2021_5d"
    #prices_path_test_data = "./data/Test_Data_Bitcoin_Market/reference_prices.csv"
    #comments_path_test = "./data/Bitcoin_Comments_2020_2021_5d"
    prices_path_test_data = "./data/Test_Data_Bitcoin_Market/reference_prices.csv"

    # comments_list
    start_date = datetime.datetime(2020, 7, 1)
    end_date = datetime.datetime(2021, 3, 8)
    file_list_test_data = create_comment_file_list(sub, start_date, end_date, interval)

    # create document list from comments
    documents_test = []
    for f in file_list_test_data:
        print("processing", f, end='\r')
        # read comment file
        comments = pd.read_csv(comments_path_test + "/" + f)
        # create document from comments and add to documents list
        documents_test.append(text_classifier.comment_df_to_document(comments))
    print('\n')
    # load prices
    prices_test = pd.read_csv(prices_path_test_data)

    # create training data
    D_test, y_test = create_training_data(documents_test, list(prices_test.price))

    # preprocess
    X_test = classifier.process_list_of_documents(D_test)
    X_test = classifier.scale_transform(X_test)

    #use_classifer_on_data(classifier.classifier, X_test, list(prices_test.price), starting_capital=10000, show=False,
    #                      name="text classifier")
    # plot_labeled_prices(prices_test, y_test, "btc-eur test", show=False)
    y_pred = classifier.classifier.predict(X_test)
    print("score for test data" , score_from_y(y_pred, y_test))

    # worth_best, hold_only = act_by_label(y_test, list(prices_test.price), starting_capital=10000, show=False, name="best case")
    date_list_test = remove_hour_minute_from_date_list(list(prices_test.date))
    worth_best, hold_only = act_by_label(y_pred, list(prices_test.price), starting_capital=10000, show=False,
                                         name="best case", ticks=date_list_test, spacing=2)
    #worth_best, hold_only = act_by_label(y_test, list(prices_test.price), starting_capital=10000, show=False, name="best case")
    #worth_classifier, hold_only = act_by_label(y_pred, list(prices_test.price), starting_capital=10000, show=True, name="text_classifier")
    plt.show()

    return worth_classifier, worth_best, hold_only


def text_classifier_with_training_Bitcoin():
    #set paths
    #prices_path_training_data = "./data/BitcoinMarkets_Data/prices.csv"
    #comments_path_train = "./data/BitcoinMarkets_Comments_2018_2020_5d"
    prices_path_training_data = "./data/Bitcoin_Data/prices.csv"
    comments_path_train = "./data/Bitcoin_Comments_2018_2020_5d"

    #comments_list
    #sub = "Bitcoin"
    sub = "BitcoinMarkets"
    interval = 5
    start_date = datetime.datetime(2018, 1, 1)
    end_date = datetime.datetime(2020, 7, 1)
    file_list_training_data = create_comment_file_list(sub, start_date, end_date, interval)

    #create document list from comments
    documents_train = []
    for f in file_list_training_data:
        print("processing", f, end='\r')
        #read comment file
        comments = pd.read_csv(comments_path_train + "/" + f)
        #create document from comments and add to documents list
        documents_train.append(text_classifier.comment_df_to_document(comments))
    print('\n')
    #create text classifier
    classifier = text_classifier.TextClassifier()

    #load prices
    prices = pd.read_csv(prices_path_training_data)

    #create training data
    D_train, y_train = create_training_data(documents_train, list(prices.price))

    #preprocess
    X_train = classifier.process_list_of_documents(D_train)

    #train classifier
    classifier.fit(X_train, y_train)

    #load test data
    # set paths
    #comments_path_test = "./data/BitcoinMarkets_Comments_2020_2021_5d"
    #prices_path_test_data = "./data/Test_Data_Bitcoin_Market/reference_prices.csv"
    comments_path_test = "./data/Bitcoin_Comments_2020_2021_5d"
    prices_path_test_data = "./data/Test_Data_Bitcoin_Market/reference_prices.csv"

    # comments_list
    start_date = datetime.datetime(2020, 7, 1)
    end_date = datetime.datetime(2021, 3, 8)
    file_list_test_data = create_comment_file_list(sub, start_date, end_date, interval)

    # create document list from comments
    documents_test = []
    for f in file_list_test_data:
        print("processing", f, end='\r')
        # read comment file
        comments = pd.read_csv(comments_path_test + "/" + f)
        # create document from comments and add to documents list
        documents_test.append(text_classifier.comment_df_to_document(comments))
    print('\n')
    # load prices
    prices_test = pd.read_csv(prices_path_test_data)

    # create training data
    D_test, y_test = create_training_data(documents_test, list(prices_test.price))

    # preprocess
    X_test = classifier.process_list_of_documents(D_test)
    X_test = classifier.scale_transform(X_test)

    #use_classifer_on_data(classifier.classifier, X_test, list(prices_test.price), starting_capital=10000, show=False,
    #                      name="text classifier")
    # plot_labeled_prices(prices_test, y_test, "btc-eur test", show=False)
    y_pred = classifier.classifier.predict(X_test)
    print("score for test data" , score_from_y(y_pred, y_test))
    act_by_label(y_test, list(prices_test.price), starting_capital=10000, show=False, name="best case")
    act_by_label(y_pred, list(prices_test.price), starting_capital=10000, show=True, name="text_classifier")
    plt.show()

def text_collector_twitter():
    # start and end date
    #training
    start_date = datetime.datetime(2019, 11, 17)
    end_date = datetime.datetime(2020, 7, 1)
    #test
    #start_date = datetime.datetime(2020, 10, 19)
    #end_date = datetime.datetime(2021, 3, 8)

    #set query
    query = "Bitcoin"

    save_path = "./data/twitter_training"
    #save_path = "./data/twitter_test"

    interval = 5
    current_date = start_date
    while (current_date < end_date):
        # offset start date by delta
        print("pulling tweets for " + datetime_to_account_format(current_date), end='\r')

        #collect tweets
        tweets_df = twitter_scraper.scrape_query_in_timeframe(query, current_date,
                                                              current_date + timedelta(days=interval), 5000)
        #save tweets
        tweets_df.to_csv(save_path + '/twitter_' + query + '_' + datetime_to_account_format(current_date) + '.csv')


        current_date = current_date + timedelta(days=interval)

def text_classifier_with_training_twitter():
    # start and end date training data
    start_date = datetime.datetime(2018, 1, 1)
    end_date = datetime.datetime(2020, 7, 1)

    # set query
    query = "Bitcoin"
    #set training data path
    save_path = "./data/twitter_training"

    #read training data and create documents
    documents_training = []
    interval = 5
    current_date = start_date
    while (current_date < end_date):
        # offset start date by delta
        print("reading tweets for " + datetime_to_account_format(current_date), end='\r')
        # save tweets
        tweets = pd.read_csv(save_path + '/twitter_' + query + '_' + datetime_to_account_format(current_date) + '.csv')
        documents_training.append(text_classifier.comment_list_to_document(list(tweets.Text)))

        current_date = current_date + timedelta(days=interval)

    #read prices
    prices_path_training_data = "./data/Bitcoin_Data/prices.csv"
    prices = pd.read_csv(prices_path_training_data)

    #create text classifier
    classifier = text_classifier.TextClassifier(path="./text_classifiers", name="text_classifier_Twitter")
    # create training data
    D_train, y_train = create_training_data(documents_training, list(prices.price))

    # preprocess
    X_train = classifier.process_list_of_documents(D_train)

    # train classifier
    classifier.fit(X_train, y_train)
    classifier.save_model()

    #read test data
    #test data path
    save_path = "./data/twitter_test"

    # start and end date test data
    start_date_test = datetime.datetime(2020, 7, 1)
    end_date_test = datetime.datetime(2021, 3, 8)
    documents_test = []
    interval = 5
    current_date = start_date_test
    while (current_date < end_date_test):
        # offset start date by delta
        print("reading tweets for " + datetime_to_account_format(current_date), end='\r')
        # save tweets
        tweets = pd.read_csv(save_path + '/twitter_' + query + '_' + datetime_to_account_format(current_date) + '.csv')
        documents_test.append(text_classifier.comment_list_to_document(list(tweets.Text)))

        current_date = current_date + timedelta(days=interval)

    #read test prices
    prices_path_test_data = "./data/Test_Data_Bitcoin_Market/reference_prices.csv"
    prices_test = pd.read_csv(prices_path_test_data)

    # create test data
    D_test, y_test = create_training_data(documents_test, list(prices_test.price))

    # preprocess
    X_test = classifier.process_list_of_documents(D_test)
    X_test = classifier.scale_transform(X_test)

    #apply classifier
    y_pred = classifier.classifier.predict(X_test)

    #show results
    print("score for test data", score_from_y(y_pred, y_test))
    worth_best, hold_only = act_by_label(y_test, list(prices_test.price), starting_capital=10000, show=False, name="best case")
    worth_classifier, hold_only = act_by_label(y_pred, list(prices_test.price), starting_capital=10000, show=True, name="text_classifier")
    plt.show()

    return worth_classifier, worth_best, hold_only

def text_classifier_load_model_twitter():
    # start and end date training data
    start_date = datetime.datetime(2018, 1, 1)
    end_date = datetime.datetime(2020, 7, 1)

    # set query
    query = "Bitcoin"
    #set training data path
    save_path = "./data/twitter_training"

    classifier = text_classifier.TextClassifier(path="./text_classifiers", name="text_classifier_Twitter")
    classifier.load_model()

    #read test data
    #test data path
    save_path = "./data/twitter_test"

    # start and end date test data
    start_date_test = datetime.datetime(2020, 7, 1)
    end_date_test = datetime.datetime(2021, 3, 8)
    documents_test = []
    interval = 5
    current_date = start_date_test
    while (current_date < end_date_test):
        # offset start date by delta
        print("reading tweets for " + datetime_to_account_format(current_date), end='\r')
        # save tweets
        tweets = pd.read_csv(save_path + '/twitter_' + query + '_' + datetime_to_account_format(current_date) + '.csv')
        documents_test.append(text_classifier.comment_list_to_document(list(tweets.Text)))

        current_date = current_date + timedelta(days=interval)

    #read test prices
    prices_path_test_data = "./data/Test_Data_Bitcoin_Market/reference_prices.csv"
    prices_test = pd.read_csv(prices_path_test_data)

    # create test data
    D_test, y_test = create_training_data(documents_test, list(prices_test.price))

    # preprocess
    X_test = classifier.process_list_of_documents(D_test)
    X_test = classifier.scale_transform(X_test)

    #apply classifier
    y_pred = classifier.classifier.predict(X_test)

    #show results
    print("score for test data", score_from_y(y_pred, y_test))
    date_list_test = remove_hour_minute_from_date_list(list(prices_test.date))
    worth_best, hold_only = act_by_label(y_pred, list(prices_test.price), starting_capital=10000, show=False,
                                         name="best case", ticks=date_list_test, spacing=2)
    #worth_best, hold_only = act_by_label(y_test, list(prices_test.price), starting_capital=10000, show=False, name="best case")
    #worth_classifier, hold_only = act_by_label(y_pred, list(prices_test.price), starting_capital=10000, show=True, name="text_classifier")
    plt.show()

    return worth_classifier, worth_best, hold_only


def text_classifier_with_training_combined():
    # start and end date training data
    start_date = datetime.datetime(2018, 1, 1)
    end_date = datetime.datetime(2020, 7, 1)

    # set query
    query = "Bitcoin"
    #set training data path
    save_path = "./data/twitter_training"

    classifier = text_classifier.TextClassifier(path="./text_classifiers", name="text_classifier_Combined")
    classifier.load_model()

    #read twitter training data and create documents
    documents_training_twitter = []
    interval = 5
    current_date = start_date
    while (current_date < end_date):
        # offset start date by delta
        print("reading tweets for " + datetime_to_account_format(current_date), end='\r')
        # save tweets
        tweets = pd.read_csv(save_path + '/twitter_' + query + '_' + datetime_to_account_format(current_date) + '.csv')
        documents_training_twitter.append(text_classifier.comment_list_to_document(list(tweets.Text)))

        current_date = current_date + timedelta(days=interval)

    #read reddit training data
    comments_path_train = "./data/BitcoinMarkets_Comments_2018_2020_5d"
    sub = "BitcoinMarkets"
    interval = 5
    start_date = datetime.datetime(2018, 1, 1)
    end_date = datetime.datetime(2020, 7, 1)
    file_list_training_data = create_comment_file_list(sub, start_date, end_date, interval)

    #create document list from comments
    documents_train_reddit = []
    for f in file_list_training_data:
        print("processing", f, end='\r')
        #read comment file
        comments = pd.read_csv(comments_path_train + "/" + f)
        #create document from comments and add to documents list
        documents_train_reddit.append(text_classifier.comment_df_to_document(comments))


    #combine twitter and reddit comments
    documents_train = []
    for i in range(len(documents_training_twitter)):
        documents_train.append(documents_training_twitter[i] + " " + documents_train_reddit[i])

    #read prices
    prices_path_training_data = "./data/Bitcoin_Data/prices.csv"
    prices = pd.read_csv(prices_path_training_data)

    #create text classifier
    classifier = text_classifier.TextClassifier(path="./text_classifiers", name="text_classifier_Combined")
    # create training data
    D_train, y_train = create_training_data(documents_train, list(prices.price))

    # preprocess
    X_train = classifier.process_list_of_documents(D_train)

    # train classifier
    classifier.fit(X_train, y_train)
    classifier.save_model()

    #read test data
    #test data path
    save_path = "./data/twitter_test"

    # start and end date test data twitter
    start_date_test = datetime.datetime(2020, 7, 1)
    end_date_test = datetime.datetime(2021, 3, 8)
    documents_test_twitter = []
    interval = 5
    current_date = start_date_test
    while (current_date < end_date_test):
        # offset start date by delta
        print("reading tweets for " + datetime_to_account_format(current_date), end='\r')
        # save tweets
        tweets = pd.read_csv(save_path + '/twitter_' + query + '_' + datetime_to_account_format(current_date) + '.csv')
        documents_test_twitter.append(text_classifier.comment_list_to_document(list(tweets.Text)))

        current_date = current_date + timedelta(days=interval)

    # start and end date test data reddit
    # load test data
    # set paths
    comments_path_test = "./data/BitcoinMarkets_Comments_2020_2021_5d"
    start_date = datetime.datetime(2020, 7, 1)
    end_date = datetime.datetime(2021, 3, 8)
    file_list_test_data = create_comment_file_list(sub, start_date, end_date, interval)

    # create document list from comments
    documents_test_reddit = []
    for f in file_list_test_data:
        print("processing", f, end='\r')
        # read comment file
        comments = pd.read_csv(comments_path_test + "/" + f)
        # create document from comments and add to documents list
        documents_test_reddit.append(text_classifier.comment_df_to_document(comments))

    # combine twitter and reddit comments
    documents_test = []
    for i in range(len(documents_test_twitter)):
        documents_test.append(documents_test_twitter[i] + " " + documents_test_reddit[i])


    #read test prices
    prices_path_test_data = "./data/Test_Data_Bitcoin_Market/reference_prices.csv"
    prices_test = pd.read_csv(prices_path_test_data)

    # create test data
    D_test, y_test = create_training_data(documents_test, list(prices_test.price))

    # preprocess
    X_test = classifier.process_list_of_documents(D_test)
    X_test = classifier.scale_transform(X_test)

    #apply classifier
    y_pred = classifier.classifier.predict(X_test)

    #show results
    print("score for test data", score_from_y(y_pred, y_test))
    worth_best, hold_only = act_by_label(y_test, list(prices_test.price), starting_capital=10000, show=False, name="best case")
    worth_classifier, hold_only = act_by_label(y_pred, list(prices_test.price), starting_capital=10000, show=True, name="text_classifier")
    plt.show()

    return worth_classifier, worth_best, hold_only

def text_classifier_load_model_combined():
    query = "Bitcoin"

    classifier = text_classifier.TextClassifier(path="./text_classifiers", name="text_classifier_Combined")
    classifier.load_model()

    #read test data
    #test data path
    save_path = "./data/twitter_test"

    # start and end date test data twitter
    start_date_test = datetime.datetime(2020, 7, 1)
    end_date_test = datetime.datetime(2021, 3, 8)
    documents_test_twitter = []
    interval = 5
    current_date = start_date_test
    while (current_date < end_date_test):
        # offset start date by delta
        print("reading tweets for " + datetime_to_account_format(current_date), end='\r')
        # save tweets
        tweets = pd.read_csv(save_path + '/twitter_' + query + '_' + datetime_to_account_format(current_date) + '.csv')
        documents_test_twitter.append(text_classifier.comment_list_to_document(list(tweets.Text)))

        current_date = current_date + timedelta(days=interval)

    # start and end date test data reddit
    # load test data
    # set paths
    comments_path_test = "./data/BitcoinMarkets_Comments_2020_2021_5d"
    start_date = datetime.datetime(2020, 7, 1)
    end_date = datetime.datetime(2021, 3, 8)
    sub = "BitcoinMarkets"
    file_list_test_data = create_comment_file_list(sub, start_date, end_date, interval)

    # create document list from comments
    documents_test_reddit = []
    for f in file_list_test_data:
        print("processing", f, end='\r')
        # read comment file
        comments = pd.read_csv(comments_path_test + "/" + f)
        # create document from comments and add to documents list
        documents_test_reddit.append(text_classifier.comment_df_to_document(comments))

    # combine twitter and reddit comments
    documents_test = []
    for i in range(len(documents_test_twitter)):
        documents_test.append(documents_test_twitter[i] + " " + documents_test_reddit[i])


    #read test prices
    prices_path_test_data = "./data/Test_Data_Bitcoin_Market/reference_prices.csv"
    prices_test = pd.read_csv(prices_path_test_data)

    # create test data
    D_test, y_test = create_training_data(documents_test, list(prices_test.price))

    # preprocess
    X_test = classifier.process_list_of_documents(D_test)
    X_test = classifier.scale_transform(X_test)

    #apply classifier
    y_pred = classifier.classifier.predict(X_test)

    #show results
    print("score for test data", score_from_y(y_pred, y_test))
    worth_best, hold_only = act_by_label(y_test, list(prices_test.price), starting_capital=10000, show=False, name="best case")
    worth_classifier, hold_only = act_by_label(y_pred, list(prices_test.price), starting_capital=10000, show=True, name="text_classifier")
    plt.show()

    return worth_classifier, worth_best, hold_only


def create_sentiment_data_twitter():
    #set paths for training data
    comment_path = "./data/twitter_training"
    save_path = "./data/twitter_sentiment"
    query ="Bitcoin"

    #set start and end date for training data
    start_date = datetime.datetime(2018, 1, 1)
    end_date = datetime.datetime(2020, 7, 1)
    interval = 5

    #create sentiment data dataframe for training data
    sentiment_textblob = pd.DataFrame(columns=['polarity', 'subjectivity'])
    sentiment_vader = pd.DataFrame(columns=['date', 'neu', 'pos', 'neg', 'compound'])

    analyser = sentiment_analyser.Analyser()
    current_date = start_date
    while (current_date < end_date):
        break
        # offset start date by delta
        print("reading tweets for " + datetime_to_account_format(current_date), end='\r')
        # save tweets
        tweets = pd.read_csv(comment_path + '/twitter_' + query + '_' + datetime_to_account_format(current_date) + '.csv')
        #perform textblob sentiment analysis
        res_tb = analyser.textblob_sentiment_analysis(list(tweets.Text), aggregate=True)
        res_tb['date'] = datetime_to_account_format(current_date)
        sentiment_textblob = sentiment_textblob.append(res_tb, ignore_index=True)
        sentiment_textblob.to_csv(save_path + '/textblob_training.csv')

        #perform vader sentiment analyis
        res_vd_raw = analyser.vader_analysis(list(tweets.Text))
        weights = []
        res_vd = analyser.aggregate_sentiments(res_vd_raw, weights)
        #save vader data
        res_vd['date'] = datetime_to_account_format(current_date)
        sentiment_vader = sentiment_vader.append(res_vd, ignore_index=True)
        sentiment_vader.to_csv(save_path + '/vader_training.csv')

        current_date = current_date + timedelta(days=interval)

    # set start and end date for test data
    start_date_test = datetime.datetime(2020, 7, 1)
    end_date_test = datetime.datetime(2021, 3, 8)
    comment_path = "./data/twitter_test"

    # create sentiment data dataframe for test data
    sentiment_textblob_test = pd.DataFrame(columns=['polarity', 'subjectivity'])
    sentiment_vader_test = pd.DataFrame(columns=['date', 'neu', 'pos', 'neg', 'compound'])

    # load and process posts for test data
    current_date = start_date_test
    while (current_date < end_date_test):
        # offset start date by delta
        print("reading tweets for " + datetime_to_account_format(current_date), end='\r')
        # save tweets
        tweets = pd.read_csv(comment_path + '/twitter_' + query + '_' + datetime_to_account_format(current_date) + '.csv')
        # perform textblob sentiment analysis
        res_tb = analyser.textblob_sentiment_analysis(list(tweets.Text), aggregate=True)
        res_tb['date'] = datetime_to_account_format(current_date)
        sentiment_textblob_test = sentiment_textblob_test.append(res_tb, ignore_index=True)
        sentiment_textblob_test.to_csv(save_path + '/textblob_test.csv')

        # perform vader sentiment analyis
        res_vd_raw = analyser.vader_analysis(list(tweets.Text))
        weights = []
        res_vd = analyser.aggregate_sentiments(res_vd_raw, weights)
        # save vader data
        res_vd['date'] = datetime_to_account_format(current_date)
        sentiment_vader_test = sentiment_vader_test.append(res_vd, ignore_index=True)
        sentiment_vader_test.to_csv(save_path + '/vader_test.csv')

        current_date = current_date + timedelta(days=interval)

    return

def vader_twitter():
    #training data pathes
    save_path = "./data/twitter_sentiment/"
    training_data_file = "vader_training.csv"
    prices_path_training_data = "./data/Bitcoin_Data/prices.csv"

    #load training data
    training_sentiments_df = pd.read_csv(save_path + training_data_file)
    prices_training = pd.read_csv(prices_path_training_data)

    #create data set for training
    #sentiment
    sentiments_training = unify_vectors_from_lists(list(training_sentiments_df.pos), list(training_sentiments_df.neg))
    sentiments_training = unify_vectors_from_lists(sentiments_training, list(training_sentiments_df.neu))
    #sentiments_training = unify_vectors_from_lists(sentiments_training, list(training_sentiments_df.compound))

    #prices
    prices_list_training = list(prices_training.price)

    #create training data
    X_train, y_train = create_training_data(sentiments_training, prices_list_training)

    #feature scaling
    scaler = StandardScaler()
    scaler.fit(X_train)
    X_train = scaler.transform(X_train)

    #train classifier
    nn = neural_net(X_train, y_train)

    #paths test data
    test_data_file = "vader_test.csv"
    prices_path_test_data = "./data/Test_Data_Bitcoin_Market/reference_prices.csv"

    #load test data
    test_sentiments_df = pd.read_csv(save_path + test_data_file)
    prices_test = pd.read_csv(prices_path_test_data)

    #creates test set
    #sentiment
    sentiments_test = unify_vectors_from_lists(list(test_sentiments_df.pos), list(test_sentiments_df.neg))
    sentiments_test = unify_vectors_from_lists(sentiments_test, list(test_sentiments_df.neu))
    #sentiments_test = unify_vectors_from_lists(sentiments_test, list(test_sentiments_df.compound))

    #prices
    prices_list_test = list(prices_test.price)

    # create test data
    X_test, y_test = create_training_data(sentiments_test, prices_list_test)

    #feature scaling
    X_test = scaler.transform(X_test)

    #test classifier
    y_pred = nn.predict(X_test)

    #evaluate/visualise results
    date_list_test = remove_hour_minute_from_date_list(list(prices_test.date))
    print("score for test data", score_from_y(y_pred, y_test))
    #worth_best, hold_only = act_by_label(y_test, list(prices_test.price), starting_capital=10000, show=False, name="best case")
    worth_best, hold_only = act_by_label(y_test, list(prices_test.price), starting_capital=10000, show=False, name="best case", ticks=date_list_test, spacing=2)
    #worth_classifier, hold_only = act_by_label(y_pred, list(prices_test.price), starting_capital=10000, show=True, name="classifier")
    plt.show()

    return worth_classifier, worth_best, hold_only

def textblob_twitter():
    # training data pathes
    save_path = "./data/twitter_sentiment/"
    training_data_file = "textblob_training.csv"
    prices_path_training_data = "./data/Bitcoin_Data/prices.csv"

    # load training data
    training_sentiments_df = pd.read_csv(save_path + training_data_file)
    prices_training = pd.read_csv(prices_path_training_data)

    # create data set for training
    # sentiment
    sentiments_training = unify_vectors_from_lists(list(training_sentiments_df.polarity), list(training_sentiments_df.subjectivity))

    # prices
    prices_list_training = list(prices_training.price)

    # create training data
    X_train, y_train = create_training_data(sentiments_training, prices_list_training)

    # feature scaling
    scaler = StandardScaler()
    scaler.fit(X_train)
    X_train = scaler.transform(X_train)

    # train classifier
    nn = neural_net(X_train, y_train)

    # paths test data
    test_data_file = "textblob_test.csv"
    prices_path_test_data = "./data/Test_Data_Bitcoin_Market/reference_prices.csv"

    # load test data
    test_sentiments_df = pd.read_csv(save_path + test_data_file)
    prices_test = pd.read_csv(prices_path_test_data)

    # creates test set
    # sentiment
    sentiments_test = unify_vectors_from_lists(list(test_sentiments_df.polarity), list(test_sentiments_df.subjectivity))

    # prices
    prices_list_test = list(prices_test.price)

    # create test data
    X_test, y_test = create_training_data(sentiments_test, prices_list_test)

    # feature scaling
    X_test = scaler.transform(X_test)

    # test classifier
    y_pred = nn.predict(X_test)

    # evaluate/visualise results
    print("score for test data", score_from_y(y_pred, y_test))
    act_by_label(y_test, list(prices_test.price), starting_capital=10000, show=False, name="best case")
    act_by_label(y_pred, list(prices_test.price), starting_capital=10000, show=True, name="text_classifier")
    plt.show()

    return

def vader_combined():
    # training data pathes
    save_path = "./data/twitter_sentiment/"
    training_data_file = "vader_training.csv"
    prices_path_training_data = "./data/Bitcoin_Data/prices.csv"

    # load training data
    training_sentiments_df_twitter = pd.read_csv(save_path + training_data_file)

    sentiment_path_training_data = "./data/BitcoinMarkets_Data/vader_sentiments.csv"
    training_sentiments_df_BitcoinMarket = pd.read_csv(sentiment_path_training_data)
    prices_training = pd.read_csv(prices_path_training_data)

    # create data set for training
    # sentiment
    sentiments_training_twitter = unify_vectors_from_lists(list(training_sentiments_df_twitter.pos),list(training_sentiments_df_twitter.neg))
    sentiments_training_twitter = unify_vectors_from_lists(sentiments_training_twitter, list(training_sentiments_df_twitter.neu))
    #sentiments_training_twitter = unify_vectors_from_lists(sentiments_training_twitter, list(training_sentiments_df_twitter.compound))

    # sentiment BitcoinMarkets
    sentiments_training_BitcoinMarkets = unify_vectors_from_lists(list(training_sentiments_df_BitcoinMarket.pos),
                                                           list(training_sentiments_df_BitcoinMarket.neg))
    sentiments_training_BitcoinMarkets = unify_vectors_from_lists(sentiments_training_BitcoinMarkets,list(training_sentiments_df_BitcoinMarket.neu))
    #sentiments_training_BitcoinMarkets = unify_vectors_from_lists(sentiments_training_BitcoinMarkets, list(training_sentiments_df_BitcoinMarket.compound))

    # combine
    sentiments_training = unify_vectors_from_lists(sentiments_training_twitter, sentiments_training_BitcoinMarkets)
    # prices
    prices_list_training = list(prices_training.price)

    # create training data
    X_train, y_train = create_training_data(sentiments_training, prices_list_training)

    # feature scaling
    scaler = StandardScaler()
    scaler.fit(X_train)
    X_train = scaler.transform(X_train)

    # train classifier
    nn = neural_net(X_train, y_train)

    # paths test data
    test_data_file = "vader_test.csv"
    prices_path_test_data = "./data/Test_Data_Bitcoin_Market/reference_prices.csv"

    # load test data
    test_sentiments_df_twitter = pd.read_csv(save_path + test_data_file)
    prices_test = pd.read_csv(prices_path_test_data)

    sentiment_path_test_data = "./data/Test_Data_Bitcoin_Market/vader_sentiments.csv"
    test_sentiments_df_BitcoinMarket = pd.read_csv(sentiment_path_test_data)

    # creates test set
    # sentiment twitter
    sentiments_test_twitter = unify_vectors_from_lists(list(test_sentiments_df_twitter.pos), list(test_sentiments_df_twitter.neg))
    sentiments_test_twitter = unify_vectors_from_lists(sentiments_test_twitter, list(test_sentiments_df_twitter.neu))
    #sentiments_test_twitter = unify_vectors_from_lists(sentiments_test_twitter, list(test_sentiments_df_twitter.compound))

    # sentiment BitcoinMarkets
    sentiments_test_BitcoinMarkets = unify_vectors_from_lists(list(test_sentiments_df_BitcoinMarket.pos),
                                                                  list(test_sentiments_df_BitcoinMarket.neg))
    sentiments_test_BitcoinMarkets = unify_vectors_from_lists(sentiments_test_BitcoinMarkets, list(test_sentiments_df_BitcoinMarket.neu))
    #sentiments_test_BitcoinMarkets = unify_vectors_from_lists(sentiments_test_BitcoinMarkets, list(test_sentiments_df_BitcoinMarket.compound))

    # combine
    sentiments_test = unify_vectors_from_lists(sentiments_test_twitter, sentiments_test_BitcoinMarkets)

    # prices
    prices_list_test = list(prices_test.price)

    # create test data
    X_test, y_test = create_training_data(sentiments_test, prices_list_test)

    # feature scaling
    X_test = scaler.transform(X_test)

    # test classifier
    y_pred = nn.predict(X_test)

    # evaluate/visualise results
    date_list_test = remove_hour_minute_from_date_list(list(prices_test.date))
    print("score for test data", score_from_y(y_pred, y_test))
    worth_best, hold_only = act_by_label(y_test, list(prices_test.price), starting_capital=10000, show=False, name="best case", ticks=date_list_test, spacing=2)
    worth_classifier, hold_only = act_by_label(y_pred, list(prices_test.price), starting_capital=10000, show=True, name="classifier", ticks=date_list_test, spacing=2)
    plt.show()
    return worth_classifier, worth_best, hold_only

def vader_BitcoinMarket():
    # training data pathes
    save_path = "./data/twitter_sentiment/"
    training_data_file = "vader_training.csv"
    prices_path_training_data = "./data/Bitcoin_Data/prices.csv"

    # load training data
    training_sentiments_df_twitter = pd.read_csv(save_path + training_data_file)

    sentiment_path_training_data = "./data/BitcoinMarkets_Data/vader_sentiments.csv"
    training_sentiments_df_BitcoinMarket = pd.read_csv(sentiment_path_training_data)
    prices_training = pd.read_csv(prices_path_training_data)


    # create data set for training
    # sentiment BitcoinMarkets
    sentiments_training_BitcoinMarkets = unify_vectors_from_lists(list(training_sentiments_df_BitcoinMarket.pos),
                                                           list(training_sentiments_df_BitcoinMarket.neg))
    sentiments_training_BitcoinMarkets = unify_vectors_from_lists(sentiments_training_BitcoinMarkets,list(training_sentiments_df_BitcoinMarket.neu))
    #sentiments_training_BitcoinMarkets = unify_vectors_from_lists(sentiments_training_BitcoinMarkets, list(training_sentiments_df_BitcoinMarket.compound))

    # prices
    prices_list_training = list(prices_training.price)

    # create training data
    X_train, y_train = create_training_data(sentiments_training_BitcoinMarkets, prices_list_training)

    # feature scaling
    scaler = StandardScaler()
    scaler.fit(X_train)
    X_train = scaler.transform(X_train)

    # train classifier
    nn = neural_net(X_train, y_train)

    # paths test data
    test_data_file = "vader_test.csv"
    prices_path_test_data = "./data/Test_Data_Bitcoin_Market/reference_prices.csv"

    # load test data
    test_sentiments_df_twitter = pd.read_csv(save_path + test_data_file)
    prices_test = pd.read_csv(prices_path_test_data)

    sentiment_path_test_data = "./data/Test_Data_Bitcoin_Market/vader_sentiments.csv"
    test_sentiments_df_BitcoinMarket = pd.read_csv(sentiment_path_test_data)

    # creates test set
    # sentiment BitcoinMarkets
    sentiments_test_BitcoinMarkets = unify_vectors_from_lists(list(test_sentiments_df_BitcoinMarket.pos),list(test_sentiments_df_BitcoinMarket.neg))
    sentiments_test_BitcoinMarkets = unify_vectors_from_lists(sentiments_test_BitcoinMarkets, list(test_sentiments_df_BitcoinMarket.neu))
    #sentiments_test_BitcoinMarkets = unify_vectors_from_lists(sentiments_test_BitcoinMarkets, list(test_sentiments_df_BitcoinMarket.compound))

    # prices
    prices_list_test = list(prices_test.price)

    # create test data
    X_test, y_test = create_training_data(sentiments_test_BitcoinMarkets, prices_list_test)

    # feature scaling
    X_test = scaler.transform(X_test)

    # test classifier
    y_pred = nn.predict(X_test)

    # evaluate/visualise results
    date_list_test = remove_hour_minute_from_date_list(list(prices_test.date))
    print("score for test data", score_from_y(y_pred, y_test))
    worth_best, hold_only = act_by_label(y_test, list(prices_test.price), starting_capital=10000, show=False, name="best case", ticks=date_list_test, spacing=2)
    worth_classifier, hold_only = act_by_label(y_pred, list(prices_test.price), starting_capital=10000, show=True, name="classifier", ticks=date_list_test, spacing=2)
    plt.show()
    return worth_classifier, worth_best, hold_only

def vis_bitcoin():
    #load prices for training period
    prices_path_training_data = "./data/Bitcoin_Data/prices.csv"
    prices_training = pd.read_csv(prices_path_training_data)

    # load prices for test period
    prices_path_test_data = "./data/Test_Data_Bitcoin_Market/reference_prices.csv"
    prices_test = pd.read_csv(prices_path_test_data)

    combined_dates = np.append(list(prices_training.date), list(prices_test.date))
    combined_prices = np.append(list(prices_training.price), list(prices_test.price))
    truncated_dates = remove_hour_minute_from_date_list(combined_dates)
    X, labels = create_training_data(combined_dates, combined_prices)

    plt.figure("Bitcoin price")
    #plot price
    plt.plot(truncated_dates, combined_prices, color="black")
    plt.ylabel("BTC price in €")
    plt.xticks(np.arange(0, len(truncated_dates) + 1, 5), rotation=-75)
    #plot labels
    set_label_buy = True
    set_label_sell = True
    for j in range(len(labels)):
        if(labels[j] == 0):
            if(set_label_sell):
                plt.scatter(j, combined_prices[j], color='r', marker='v', label='sell')
                set_label_sell = False
            else:
                plt.scatter(j, combined_prices[j], color='r', marker='v')
        else:
            if(set_label_buy):
                plt.scatter(j, combined_prices[j], color='g', marker='^', label='buy')
                set_label_buy = False
            else:
                plt.scatter(j, combined_prices[j], color='g', marker='^')
    vline_index = len(list(prices_training.date))
    plt.text(truncated_dates[10], np.max(combined_prices)-100, "Training Data")
    plt.text(truncated_dates[vline_index+10], np.max(combined_prices)-100, "Test Data")
    plt.vlines(truncated_dates[vline_index], np.min(combined_prices), np.max(combined_prices), linestyles="dashed")
    plt.legend()

    plt.show()


if __name__ == "__main__":
    #vader()
    #textblob()
    #text_classifier_with_training_Bitcoin()
    #text_collector_twitter()
    #create_sentiment_data_twitter()
    #textblob_twitter()
    #worth_classifier_BitcoinMarket, worth_best, hold_only = vader_BitcoinMarket()
    worth_classifier_twitter, worth_best, hold_only = vader_twitter()
    #worth_classifier_combined, worth_best, hold_only = vader_combined()
    #worth_classifier_text_BitcoinMarkets, worth_best, hold_only = text_classifier_with_training_BitcoinMarket()
    #worth_classifier_text_BitcoinMarkets, worth_best, hold_only = text_classifier_load_model_BitcoinMarket()
    #worth_classifier_text_BitcoinMarkets, worth_best, hold_only = text_classifier_load_model_twitter()
    #worth_classifier_text_twitter, worth_best, hold_only = text_classifier_with_training_twitter()
    #worth_classifier_text_BitcoinMarkets, worth_best, hold_only = text_classifier_with_training_BitcoinMarket()
    #worth_classifier_text_twitter, worth_best, hold_only = text_classifier_with_training_twitter()
    #text_classifier_with_training_combined()
    #text_classifier_load_model_combined()

    #get dates
    prices_test = pd.read_csv("./data/Test_Data_Bitcoin_Market/reference_prices.csv")
    dates = list(prices_test.date)


    plt.figure("Financial Gain")
    plt.plot(dates[:len(worth_best)], worth_best, color="black", linestyle='--')
    plt.plot(dates[:len(hold_only)], hold_only, color="red", linestyle=':')
    plt.plot(dates[:len(worth_classifier_twitter)], worth_classifier_BitcoinMarket[:len(worth_classifier_twitter)], label="VADER - r/Bitcoinmarkets")
    plt.plot(dates[:len(worth_classifier_twitter)], worth_classifier_twitter, label="VADER - Twitter")
    plt.plot(dates[:len(worth_classifier_combined)], worth_classifier_combined, label="VADER - Twitter + r/BitcoinMarkets")
    #plt.plot(dates[:len(worth_classifier_text_BitcoinMarkets)], worth_classifier_text_BitcoinMarkets, label="BUY/SELL-Classifier - r/BitcoinMarkets")
    #plt.plot(dates[:len(worth_classifier_text_twitter)], worth_classifier_text_twitter, label="BUY/SELL-Classifier - Twitter")

    plt.legend()
    plt.ylabel("Euro [€]")
    plt.xticks(np.arange(0, len(dates) + 1, 2), rotation=-75)
    plt.annotate("Best Case", (dates[len(worth_best)-1], worth_best[-1]))
    plt.annotate("Buy once and wait", (dates[len(worth_best)-1], hold_only[-1]))
    '''
    plt.annotate("VADER - r/Bitcoinmarkets", (dates[len(worth_best)-1], worth_classifier_BitcoinMarket[len(worth_classifier_twitter)-1]))
    plt.annotate("VADER - Twitter", (dates[len(worth_best)-1], worth_classifier_twitter[-1]))
    plt.annotate("VADER - Twitter + r/BitcoinMarkets", (dates[len(worth_best)-1], worth_classifier_combined[-1]))
    plt.annotate("BUY/SELL-Classifier - r/BitcoinMarkets", (dates[len(worth_best)-1], worth_classifier_text_BitcoinMarkets[-1]))
    plt.annotate("BUY/SELL-Classifier - Twitter", (dates[len(worth_best)-1], worth_classifier_text_twitter[-1]))
    '''

    plt.gca().spines['top'].set_visible(False)
    plt.gca().spines['right'].set_visible(False)

    plt.show()

    #vis_bitcoin()

