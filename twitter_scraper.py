'''
@author Julian Christopher Kerl
'''
# importing libraries and packages
import snscrape.modules.twitter as sntwitter
import pandas as pd
import datetime
from datetime import timedelta

def date_to_string(date):
    '''@brief Returns a string for a given date'''
    #create the date string. Skip the last three characters (seconds)
    s = str(date)[:-3]
    #replace spaces
    s = s.replace(" ", "-")
    #replace :
    s = s.replace(":", "-")

    return s

def scrape_query_in_timeframe(search_query, start_time, end_time, number_of_tweets=500):
    """!@brief returns a dataframe of number_of_tweets search results in given time frame"""
    #check date format
    if(type(end_time) == str):
        end = end_time
    else:
        end = date_to_string(end_time)

    if (type(start_time) == str):
        start = start_time
    else:
        start = date_to_string(start_time)

    #create search query
    query = search_query + ' since:' + start + " until:" + end
    # Creating list to append tweet data to
    tweets_list = []

    # Using TwitterSearchScraper to scrape data and append tweets to list
    for i, tweet in enumerate(
            sntwitter.TwitterSearchScraper(query).get_items()):
        if i > number_of_tweets:
            break
        tweets_list.append([tweet.date, tweet.id, tweet.content])


    # Creating a dataframe from the tweets list above
    tweets_df = pd.DataFrame(tweets_list, columns=['Datetime', 'Tweet Id', 'Text'])
    print("found", len(tweets_list), "tweets                   ", end='\r')

    return tweets_df

if __name__ == "__main__":
    # Creating list to append tweet data to
    tweets_list2 = []

    # Using TwitterSearchScraper to scrape data and append tweets to list
    for i, tweet in enumerate(
            sntwitter.TwitterSearchScraper('Bitcoin since:2021-01-01 until:2021-05-31').get_items()):
        if i > 500:
            break
        tweets_list2.append([tweet.date, tweet.id, tweet.content, tweet.username])
        #print(tweet.content)


    # Creating a dataframe from the tweets list above
    tweets_df2 = pd.DataFrame(tweets_list2, columns=['Datetime', 'Tweet Id', 'Text', 'Username'])
