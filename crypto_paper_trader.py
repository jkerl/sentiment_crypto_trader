'''
@author: Julian Christopher Kerl

Simple paper trader for crypto currencies using David Woroniuk's Historic_Crypto package
https://github.com/David-Woroniuk/Historic_Crypto
'''

#Imports
#cryto related packages
from Historic_Crypto import HistoricalData
from Historic_Crypto import Cryptocurrencies
from Historic_Crypto import LiveCryptoData

#data processing related
import pandas as pd
import numpy as np


class order():
    '''@brief Class encoding a buy- or sell-order'''
    def __init__(self, order_type, symbol, amount, execution_time, upper_limit = np.inf, lower_limit= -1 * np.inf):
        '''@brief order_type: buy/sell | symbol:The ticker symbol of the coin to buy | amount: how many coins to by |
        execution_time: When the trade will be executed in format yyyy-mm-dd-hh-mm'''
        self.order_type = order_type
        self.symbol = symbol
        self.amount = amount
        self.upper_limit = upper_limit
        self.lower_limit = lower_limit
        self.execution_time = execution_time


class Account():
    def __init__(self, save_path, name):
        self.save_path = save_path
        self.buying_power = 0
        self.name = name
        self.wallet = pd.DataFrame(columns=['symbol', 'amount', 'purchase_value'])
        self.wallet.set_index("symbol")

    def save(self):
        '''@brief saves the account'''
        self.wallet.to_csv(self.save_path + '/' + self.name + '_account.csv')
        df = pd.DataFrame(np.array([[self.buying_power]]), columns=['buying_power'])
        df.to_csv(self.save_path + '/' + self.name + '_buying_power.csv')


    def load(self):
        '''@brief loads account from save path'''
        self.wallet = pd.read_csv(self.save_path + '/' + self.name + '_account.csv')
        df = pd.read_csv(self.save_path + '/' + self.name + '_buying_power.csv')
        self.buying_power = df.loc[0, 'buying_power']
        print(self.wallet)

    def deposit(self, amount):
        '''@brief deposit given amount to account'''
        self.buying_power += amount

    def withdraw(self, amount):
        '''@brief withdraw given amount to account'''
        self.buying_power -= amount

    def get_price(self, symbol, time):
        '''@brief Returns the price of a given symbol at a given time '''
        minutes = np.int(time[-2])
        # Get the execution time in a format for HistoricalData
        if (minutes == 59):
            start_date = time
            start_date = start_date[:-2] + '50'
            end_date = time
        else:
            start_date = time
            end_date = time
            end_date = end_date[:-2] + str(minutes + 10)

        # get the coin price for the order
        data = HistoricalData(symbol, 300, start_date=start_date, end_date=end_date).retrieve_data()
        coin_price = np.float(data[['close']].iloc[0])

        return coin_price

    def execute_order(self, order):
        '''@brief Executes buy- or sell- orders'''
        minutes = np.int(order.execution_time[-2])
        #Get the execution time in a format for HistoricalData
        if(minutes == 59):
            start_date = order.execution_time
            start_date = start_date[:-2] + '50'
            end_date = order.execution_time
        else:
            start_date = order.execution_time
            end_date = order.execution_time
            end_date = end_date[:-2] + str(minutes + 10)

        #get the coin price for the order
        data = HistoricalData(order.symbol, 300, start_date=start_date, end_date=end_date).retrieve_data()
        coin_price = np.float(data[['close']].iloc[0])

        if(order.order_type == 'buy'):
            #check if the current buying power is sufficient to buy the desired amount of coins, if not, buy as much as possible
            if(self.buying_power - order.amount * coin_price < 0):
                new_amount = self.buying_power / coin_price
            else:
                new_amount = order.amount

            #calculate order price
            order_price = new_amount * coin_price

            print("buying " + str(new_amount) + " of " + order.symbol + " for " + str(order_price))

            #check if the account already has a position of the current coin. If so, add to this position, otherwise create a new entry
            if (order.symbol in np.array(self.wallet[['symbol']])):
                # 'symbol', 'amount', 'purchase_value', 'buying_date'
                # get new amount
                current_amount = self.wallet.loc[self.wallet['symbol'] == order.symbol, ['amount']]
                self.wallet.loc[self.wallet['symbol'] == order.symbol, ['amount']] = current_amount + new_amount

                # get new purchase value
                current_purchase_value = self.wallet.loc[self.wallet['symbol'] == order.symbol, ['purchase_value']]
                self.wallet.loc[self.wallet['symbol'] == order.symbol, ['purchase_value']] = current_purchase_value + order_price

                # get new buying date
                self.wallet.loc[self.wallet['symbol'] == order.symbol, ['buying_date']].append(order.execution_time)


            else:
                # Create new wallet entry
                new_coin = {'symbol': order.symbol, 'amount': new_amount, 'purchase_value': order_price,
                            'buying_date': [order.execution_time]}
                self.wallet = self.wallet.append(new_coin, ignore_index=True)
            self.withdraw(order_price)

        elif (order.order_type == 'sell'):

            # check if the account already has a position of the current coin. If so, add to this position, otherwise create a new entry
            if (order.symbol in np.array(self.wallet[['symbol']])):
                # 'symbol', 'amount', 'purchase_value', 'buying_date'
                # get new amount
                current_amount = self.wallet.loc[self.wallet['symbol'] == order.symbol, ['amount']].amount[0]
                print(current_amount)

                #check if current amount smaller or equal to the amount of coins that are to be sold. if not, sell all
                if(order.amount >= current_amount):
                    amount_to_sell = current_amount
                    #remove entry from wallet because all coins are sold
                    self.wallet = self.wallet[self.wallet.symbol != order.symbol]

                    # calculate revenue
                    revenue = amount_to_sell * coin_price

                else:
                    amount_to_sell = order.amount

                    #calculate revenue
                    revenue = amount_to_sell * coin_price

                    self.wallet.loc[self.wallet['symbol'] == order.symbol, ['amount']] = current_amount - amount_to_sell

                    # get new purchase value
                    current_purchase_value = self.wallet.loc[self.wallet['symbol'] == order.symbol, ['purchase_value']]
                    self.wallet.loc[self.wallet['symbol'] == order.symbol, ['purchase_value']] = current_purchase_value - revenue

                self.deposit(revenue)