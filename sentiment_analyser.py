'''
@author Julian Christopher Kerl
Reddit scraping functions for crypto paper trader
'''
import praw
from psaw import PushshiftAPI
import pandas as pd
import numpy as np
import json
import csv
import time
import datetime
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer #VADER sentiment model
from flair.models import TextClassifier
from flair.data import Sentence
from textblob import TextBlob
import matplotlib.pyplot as plt

class Analyser():
    def __init__(self):
        self.vader = SentimentIntensityAnalyzer()

    def vader_analysis(self, comment):
        '''@brief performs a sentiment analysis using the VADER sentiment intensity analyser for a single comment or a
        list of comments and returns the result as a pandas DataFrame'''
        #'pos': 0.746, 'compound': 0.8316, 'neu': 0.254, 'neg': 0.0
        sentiments = pd.DataFrame(columns=['pos', 'compound', 'neu', 'neg'])

        #check if comment is a list or a single string and process accordingly
        if(isinstance(comment, list)):
            for c in comment:
                if(type(c) != str):
                    print("non string")
                    continue
                sentiments = sentiments.append(self.vader.polarity_scores(c), ignore_index=True)
        else:
            sentiments = sentiments.append(self.vader.polarity_scores(comment), ignore_index=True)

        return sentiments

    def aggregate_sentiments(self, sentiments, weights):
        '''@brief aggregates a given DataFrame of sentiments using a given set of weight. Uses DataFrame
        of form ['pos', 'compound', 'neu', 'neg']. If weights is empty, the mean of all colums is returned'''
        aggregated_sentiments = pd.DataFrame(columns=['pos', 'compound', 'neu', 'neg'])
        if(len(weights) < 1):
            pos = np.mean(list(sentiments.pos))
            neg = np.mean(list(sentiments.neg))
            neu = np.mean(list(sentiments.neu))
            compound = np.mean(list(sentiments.compound))
        else:
            #compute weighted average
            pos = np.sum(np.multiply(weights, list(sentiments.pos))) / np.sum(weights)
            neg = np.sum(np.multiply(weights, list(sentiments.neg))) / np.sum(weights)
            neu = np.sum(np.multiply(weights, list(sentiments.neu))) / np.sum(weights)
            compound = np.sum(np.multiply(weights, list(sentiments.compound))) / np.sum(weights)

        #aggregated_sentiments = aggregated_sentiments.append({'pos':pos, 'compound':compound, 'neu':neu, 'neg':neg})

        return {'pos':pos, 'compound':compound, 'neu':neu, 'neg':neg}

    def flair_sentiment_analysis(self, comment):

        classifier = TextClassifier.load('en-sentiment')
        # check if comment is a list or a single string and process accordingly
        if (isinstance(comment, list)):
            for c in comment:
                if (type(c) != str):
                    print("non string")
                    continue
            sentence = Sentence(c)
            res = classifier.predict(sentence)
            print(res)
            testimonial = TextBlob(c)
            print(testimonial.sentiment)
        else:
            sentence = Sentence(c)
            res = classifier.predict(sentence)
            print(res)
            testimonial = TextBlob(c)
            print(testimonial.sentiment)

            def flair_sentiment_analysis(self, comment):

                classifier = TextClassifier.load('en-sentiment')
                # check if comment is a list or a single string and process accordingly
                if (isinstance(comment, list)):
                    for c in comment:
                        if (type(c) != str):
                            print("non string")
                            continue
                    sentence = Sentence(c)
                    res = classifier.predict(sentence)
                    print(res)
                    testimonial = TextBlob(c)
                    print(testimonial.sentiment)
                else:
                    sentence = Sentence(c)
                    res = classifier.predict(sentence)
                    print(res)
                    testimonial = TextBlob(c)
                    print(testimonial.sentiment)

    def textblob_sentiment_analysis(self, comment, aggregate=False):
        # check if comment is a list or a single string and process accordingly

        sentiment = pd.DataFrame(columns=('polarity', 'subjectivity'))
        if (isinstance(comment, list)):
            for c in comment:
                if (type(c) != str):
                    print("non string")
                    continue
                testimonial = TextBlob(c)
                sentiment = sentiment.append({'polarity':testimonial.sentiment.polarity,
                                              'subjectivity':testimonial.sentiment.subjectivity}, ignore_index=True)
            if(aggregate):
                polarity_mean = np.mean(list(sentiment.polarity))
                subjectivity_mean = np.mean(list(sentiment.subjectivity))
                sentiment = pd.DataFrame(columns=('polarity', 'subjectivity'))
                sentiment = sentiment.append({'polarity': polarity_mean,
                                              'subjectivity': subjectivity_mean}, ignore_index=True)

        else:
            testimonial = TextBlob(c)
            sentiment = sentiment.append({'polarity': testimonial.sentiment.polarity,
                                          'subjectivity': testimonial.sentiment.subjectivity}, ignore_index=True)

        return sentiment