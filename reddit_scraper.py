'''
@author Julian Christopher Kerl
Reddit scraping functions for crypto paper trader
'''
import praw
from psaw import PushshiftAPI
import pandas as pd
import numpy as np
import requests
import json
import csv
import time
import datetime
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer #VADER sentiment model
import matplotlib.pyplot as plt

#User date (removed)
user_agent = '...'
client_id = '...'
secret = '...'

def getPushshiftData(query, after, before, sub, size=-1):
    '''@brief Function that queries the PushShift api for a subreddit sub in a timeframe given by before and after
    and returns size number of submissions. If size == -1, all submissions are returned
    adapted from https://medium.com/analytics-vidhya/sentiment-analysis-for-trading-with-reddit-text-data-73729c931d01'''

    if(size == -1):
        url = 'https://api.pushshift.io/reddit/search/submission/?title='+str(query)+'&after='+str(after)+'&before='+str(before)+'&subreddit='+str(sub)
    else:
        url = 'https://api.pushshift.io/reddit/search/submission/?title='+str(query)+'&size=' + str(size) + '&after='+str(after)+'&before='+str(before)+'&subreddit='+str(sub)

    print(url)
    r = requests.get(url)
    data = json.loads(r.text)
    return data['data']

def extract_all_comments(submission):
    '''@brief extracts all comments from a given submission and returns them as a pandas DataFrame. Includes the submission text.'''
    #Create DataFrame
    comments_all = pd.DataFrame(columns=['id', 'submission_id', 'score', 'body'])

    #chack if submission is empty, otherwise save submission as "top-level" comment
    if (not (submission.selftext == '[removed]' or submission.selftext == '[deleted]')):
        c = {'id':submission.id, 'submission_id':submission.id, 'score':submission.score, 'body':submission.title + ". " + submission.selftext}
        comments_all = comments_all.append(c, ignore_index=True)

    #Extract comments
    #Flatten tree
    submission.comments.replace_more(limit=None)
    nr = 0
    for comment in submission.comments.list():
        # print('processing ' + str(nr) + ' of ' + str(len(submission.comments.list())) + ' comments', end='\r')
        nr = nr + 1
        #get comment body and skip if the comment was removed
        body = comment.body
        if(body == '[removed]' or body == '[deleted]'):
            continue

        #save comment to DataFrame
        c = {'id':comment.id, 'submission_id':submission.id, 'score':comment.score, 'body':body}
        comments_all = comments_all.append(c, ignore_index=True)
    #print(len(comments_all), end='\r')
    #print(str(len(comments_all)) + " comments for current submission")
    return comments_all

class PSAWDataCollector():
    def __init__(self, reddit):
        self.api = PushshiftAPI(reddit)

    def query(self, after, before, sub, limit=-1, query=""):
        '''@brief Query the PushShift api for a specific subreddit in a given timeframe. Additionally, a limit for the
        number of responses and a specific query can be specified'''

        if(limit <= 0 and len(query) < 1):
            response = self.api.search_submissions(after=after, before=before, subreddit=sub, sort='desc')
        elif(limit > 0 and len(query) < 1):
            response = self.api.search_submissions(after=after, before=before, subreddit=sub, limit=limit, sort='desc')
        elif(limit <= 0 and len(query) > 0):
            response = self.api.search_submissions(after=after, before=before, subreddit=sub, sort='desc', query=query)
        else:
            response = self.api.search_submissions(after=after, before=before, subreddit=sub, limit=limit, sort='desc', query=query)

        return response

    def posts_in_timeframe(self, after, before, sub, limit=-1, query=""):
        '''@brief Returns all submissions and comments made in a given time frame from a subreddit as a DataFrame'''
        #get submissions
        submissions = self.query(after, before, sub, limit=limit, query="")
        submissions_list = list(submissions)
        print(str(len(submissions_list)) + " submissions found")
        #Create unified DataFrame for all comments
        comments = pd.DataFrame(columns=['id', 'submission_id', 'score', 'body'])
        nr = 0
        for s in submissions_list:
            print('processing submission ' + str(nr) + ' of ' + str(len(submissions_list)), end='\r')
            nr = nr+1
            c = extract_all_comments(s)
            #print(str(len(c))+' comments for current submission', end='\r')
            comments = comments.append(c)
        print(str(len(comments)) + " comments for current query")
        return comments

if __name__ == "__main__":
    reddit = praw.Reddit(client_id=client_id, client_secret=secret, user_agent=user_agent)
    # Subreddit to query
    sub = 'BitcoinMarkets'
    # before and after dates
    before = "1594339200"  # july 10 2020
    after = "1498867200"  # july 1 2017
    # query string
    query = "Daily Discussion Thread"

    after = int(datetime.datetime(2020, 7, 20).timestamp())
    before = int(datetime.datetime(2020, 7, 30).timestamp())
    print(before, after)
    #data = getPushshiftData(query, after, before, sub, size=100)
    #print(data)
    collector = PSAWDataCollector(reddit)
    gen = collector.query(after, before, sub, limit=500)
    l = list(gen)
    df = pd.DataFrame(columns=['id', 'submission_id', 'score', 'body'])
    for s in l:
        c = extract_all_comments(s)
        df = df.append(c)

    sentiment_analyser = SentimentIntensityAnalyzer()
    comment_list = list(df.body)
    score_list = list(df.score)
    sentiment_list = []
    for c in comment_list:
        #print(sentiment_analyser.polarity_scores(c))
        sentiment_list.append(sentiment_analyser.polarity_scores(c)['compound'])
    print("mean of sentiment_list: " + str(np.mean(sentiment_list)))
    weight_sentiment = np.multiply(score_list, sentiment_list)
    plt.figure('collective compound')
    plt.plot(sentiment_list)
    plt.figure('collective weighted compound')
    plt.plot(weight_sentiment)
    plt.show()

    df.to_csv('./all_comments.csv', sep=',')
    '''
        s.comments.replace_more(limit=None)
        for comment in s.comments.list():
            print(comment.body)
            print(comment.id)
    print(l)
'''