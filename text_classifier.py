'''
@author: Julian Christopher Kerl
Based on https://stackabuse.com/text-classification-with-python-and-scikit-learn/
'''

#import packages for text classification
import numpy as np
import re
import nltk
from sklearn.datasets import load_files
import pickle
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score

def comment_df_to_document(comments):
    """!@brief Concatenates all posts from a comments dataframe into single string"""
    comment_list = list(comments.body)

    #create document to collect all comments
    document = ""

    #concatenate all strings to document
    for c in comment_list:
        document = document + " " + str(c)

    return document

def comment_list_to_document(comments):
    """!@brief Concatenates all posts from a comments dataframe into single string"""

    #create document to collect all comments
    document = ""

    #concatenate all strings to document
    for c in comments:
        document = document + " " + str(c)

    return document


class TextClassifier:
    def __init__(self, path=".", name="text_classifier"):
        self.classifier_path = path
        self.name = name
        self.classifier = None
        self.scaler = None

    def save_model(self):
        """!@brief save model to self.path"""
        #save model
        with open(self.classifier_path + "/" +self.name, 'wb') as picklefile:
            pickle.dump(self.classifier, picklefile)
        #save scaler
        with open(self.classifier_path + "/" +self.name + "_scaler", 'wb') as picklefile:
            pickle.dump(self.scaler, picklefile)

    def load_model(self):
        """!@brief LOads saved scikit learn classifier model"""
        #load model
        with open(self.classifier_path + "/" + self.name, 'rb') as training_model:
            self.classifier = pickle.load(training_model)
        #load scaler
        with open(self.classifier_path + "/" + self.name + "_scaler", 'rb') as scaler:
            self.scaler = pickle.load(scaler)

    def preprocess_single_document(self, raw_document):
        """ !@brief preprocesses a single document for use in text classifier.
        Based on https://stackabuse.com/text-classification-with-python-and-scikit-learn/ """
        #Create stemmer for stemming and lemmatization
        stemmer = WordNetLemmatizer()

        # Remove all the special characters
        preprocessed_document = re.sub(r'\W', ' ', raw_document)

        # remove all single characters
        preprocessed_document = re.sub(r'\s+[a-zA-Z]\s+', ' ', preprocessed_document)

        # Remove single characters from the start
        preprocessed_document = re.sub(r'\^[a-zA-Z]\s+', ' ', preprocessed_document)

        # Substituting multiple spaces with single space
        preprocessed_document = re.sub(r'\s+', ' ', preprocessed_document, flags=re.I)

        # Removing prefixed 'b'
        preprocessed_document = re.sub(r'^b\s+', '', preprocessed_document)

        # Converting to Lowercase
        preprocessed_document = preprocessed_document.lower()

        # Lemmatization
        preprocessed_document = preprocessed_document.split()

        preprocessed_document = [stemmer.lemmatize(word) for word in preprocessed_document]
        preprocessed_document = ' '.join(preprocessed_document)

        return preprocessed_document

    def process_list_of_documents(self, list_of_docs):
        """!@brief Processes list of documents to create usable data for text classifier"""
        #create list for preprocessed documents
        documents = []
        #process all given documents
        i = 0
        for d in list_of_docs:
            print("processing ", i, "of", len(list_of_docs), end='\r')
            documents.append(self.preprocess_single_document(d))
            i = i + 1

        #vectorize processed documents (Bag of words)
        vectorizer = CountVectorizer(max_features = 2000, min_df=5, max_df=0.8, stop_words=stopwords.words('english'))
        #vectorizer = CountVectorizer(max_features=1500, min_df=0.5, max_df=0.7, stop_words=stopwords.words('english'))
        X = vectorizer.fit_transform(documents).toarray()

        #tfidf transform
        tfidfconverter = TfidfTransformer()
        X = tfidfconverter.fit_transform(X).toarray()

        return X

    def fit(self,X,y):
        """!@brief Fits Random Forest classifier to given training data"""
        X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, random_state=1, test_size=0.15)
        #classifier = RandomForestClassifier(n_estimators=1000, random_state=0)
        #classifier.fit(X_train, y_train)

        #Feature scaling
        from sklearn.preprocessing import StandardScaler
        from sklearn.neural_network import MLPClassifier

        #train scaler
        scaler = StandardScaler()
        scaler.fit(X_train)
        self.scaler = scaler

        #Apply transformation
        X_train = self.scale_transform(X_train)
        X_test = self.scale_transform(X_test)




        classifier = MLPClassifier(alpha=0.0001, random_state=1, max_iter=1000, solver='lbfgs').fit(X, y)
        #evaluate performance
        y_pred = classifier.predict(X_test)
        print("Text classifier", classifier.score(X_test, y_test))
        print(confusion_matrix(y_test, y_pred))
        print(classification_report(y_test, y_pred))
        print(accuracy_score(y_test, y_pred))
        self.classifier = classifier

    def scale_transform(self, X):
        """!@brief Applies transformation to data (Feature Scaling)"""
        #apply transformation
        X_out = self.scaler.transform(X)
        return X_out


if __name__ == "__main__":
    pass

